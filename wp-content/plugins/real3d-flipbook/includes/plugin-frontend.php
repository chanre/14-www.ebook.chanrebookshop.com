<?php 
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

function real3dflipbook_shortcode($atts){

	$args = shortcode_atts( 
		array(
			'id'   => '-1',
			'name' => '-1',
			'pdf' => '-1',
			'mode' => '-1',
			'viewmode' => '-1',
			'lightboxopened' => '-1',
			'lightboxfullscreen' => '-1',
			'lightboxtext' => '-1',
			'lightboxcssclass' => '-1',
			'lightboxthumbnail' => '-1',
			'lightboxthumbnailurl' => '-1',
			'hidemenu' => '-1',
			'autoplayonstart' => '-1',
			'autoplayinterval' => '-1',
			'zoom' => '-1',
			'zoomdisabled' => '-1',
			'btndownloadpdfurl' => '-1',
			'aspect' => '-1',

			'thumb' => '-1',
			'thumbcss' => '-1',
			'containercss' => '-1',

			'tilt' => '-1',
			'tiltmin' => '-1',
			'tiltmax' => '-1',
			'lights' => '-1',
			'shadows' => '-1',
			'pageroughness' => '-1',
			'pagemetalness' => '-1',
			'pagehardness' => '-1',
			'coverhardness' => '-1',

			'singlepage' => '-1',

			'deeplinkingprefix' => '-1'

		), 
		$atts
	);

	if($args['id'] == "all"){

		$output = '<div></div>';

		$real3dflipbooks_ids = get_option('real3dflipbooks_ids');

		foreach ($real3dflipbooks_ids as $id) {

			$shortcode = '[real3dflipbook id="'.$id.'" mode="lightbox"';

			if($args['thumbcss'] != -1)
				$shortcode .= ' thumbcss="'.$args['thumbcss'].'"';

			if($args['containercss'] != -1)
				$shortcode .= ' containercss="'.$args['containercss'].'"';

			$shortcode .= ']';

			$output .= do_shortcode($shortcode);

		}

		return $output;

	}
	
	$id = (int) $args['id'];
	$name = $args['name'];

	if($name != -1){
		$real3dflipbooks_ids = get_option('real3dflipbooks_ids');
		foreach ($real3dflipbooks_ids as $id) {
			$book = get_option('real3dflipbook_'.$id);
			if($book && $book['name'] == $name){
				$flipbook = $book;
				$id = $flipbook['id'];
				break;
			}
		}
	}else if($id != -1){
		$flipbook = get_option('real3dflipbook_'.$id);
	}else{
		$flipbook = array();
		$id = '0';
	}
	
	$bookId = $id .'_'.uniqid();

	foreach ($args as $key => $val) {
		if($val != -1){

			if($key == 'mode') $key = 'mode';
			if($key == 'viewmode') $key = 'viewMode';
			
			if($key == 'pdf') $key = 'pdfUrl';
			if($key == 'btndownloadpdfurl') $key = 'btnDownloadPdfUrl';
			
			if($key == 'hidemenu') $key = 'hideMenu';
			if($key == 'autoplayonstart') $key = 'autoplayOnStart';
			if($key == 'autoplayinterval') $key = 'autoplayInterval';
			if($key == 'zoom') $key = 'zoomLevels';
			if($key == 'zoomisabled') $key = 'zoomDisabled';

			if($key == 'lightboxtext') $key = 'lightboxText';
			if($key == 'lightboxcssclass') $key = 'lightboxCssClass';
			if($key == 'lightboxthumbnailurl') $key = 'lightboxThumbnailUrl';
			if($key == 'lightboxopened') $key = 'lightBoxOpened';
			if($key == 'lightboxfullscreen') $key = 'lightBoxFullscreen';

			if($key == 'tiltmin') $key = 'tiltMin';
			if($key == 'tiltmax') $key = 'tiltMax';
			if($key == 'pageroughness') $key = 'pageRoughness';
			if($key == 'pagemetalness') $key = 'pageMetalness';
			if($key == 'pagehardness') $key = 'pageHardness';
			if($key == 'coverhardness') $key = 'coverHardness';

			if($key == 'aspect') {

				$key = 'aspectRatio';
				$flipbook['responsiveHeight'] = 'true';

			}

			if($key == 'thumb') $key = 'lightboxThumbnailUrl';
			if($key == 'thumbcss') $key = 'lightboxThumbnailUrlCSS';
			if($key == 'containercss') $key = 'lightboxContainerCSS';

			if($key == 'singlepage') $key = 'singlePageMode';

			if($key == 'deeplinkingprefix') {
				$flipbook['deeplinkingEnabled'] = 'true';
				$flipbook['deeplinkingPrefix'] = $val;
				unset($flipbook['deeplinking']);
			}

	    	$flipbook[$key] = $val;
		}
	}

	$flipbook['rootFolder'] = plugins_url()."/real3d-flipbook/";

	$flipbook_global_options = get_option("real3dflipbook_global", array());

	$flipbook = array_merge($flipbook_global_options, $flipbook);

	// $output = '<div class="real3dflipbook" id="'.$bookId.'">';

	// $output = '<div class="bla real3dflipbook" id="'.$bookId.'" style="'.$flipbook['lightboxContainerCSS'].';">';
	$output = '<div class="real3dflipbook" id="'.$bookId.'" style="position:absolute;">';

	// if($flipbook['lightboxThumbnailUrl']){
	// 	$output .= '<img src="'.esc_html($flipbook['lightboxThumbnailUrl']).'" style="'.esc_html($flipbook['lightboxThumbnailUrlCSS']).'">';
	// }

	// if($flipbook['lightboxText']){
	// 	$output .= '<a href="#" style="'.esc_html($flipbook['lightboxTextCSS']).'">'.esc_html($flipbook['lightboxText']).'</a>';
	// }
	
	$output .= '</div>';
	// $output = "<div class='real3dflipbook' id='".$bookId."' data-flipbook-options='".json_encode($flipbook)."'></div>";

     if (!wp_script_is( 'real3d_flipbook', 'enqueued' )) {
     	wp_enqueue_script("real3d_flipbook", plugins_url()."/real3d-flipbook/js/flipbook.min.js", array('jquery'),REAL3D_FLIPBOOK_VERSION);
     }

     if (!wp_script_is( 'real3d_flipbook_book3', 'enqueued' )) {
     	wp_enqueue_script("real3d_flipbook_book3", plugins_url()."/real3d-flipbook/js/flipbook.book3.min.js", array('real3d_flipbook'),REAL3D_FLIPBOOK_VERSION);
     }

     if (!wp_script_is( 'real3d_flipbook_bookswipe', 'enqueued' )) {
     	wp_enqueue_script("real3d_flipbook_bookswipe", plugins_url()."/real3d-flipbook/js/flipbook.swipe.min.js", array('real3d_flipbook'),REAL3D_FLIPBOOK_VERSION);
     }

     if (!wp_script_is( 'iscroll', 'enqueued' )) {
     	wp_enqueue_script("iscroll", plugins_url()."/real3d-flipbook/js/iscroll.min.js", array('real3d_flipbook'),REAL3D_FLIPBOOK_VERSION);
     }

     if($flipbook['viewMode'] == 'webgl'){
	     if (!wp_script_is( 'real3d_flipbook_threejs', 'enqueued' )) {
	     	wp_enqueue_script("real3d_flipbook_threejs", plugins_url()."/real3d-flipbook/js/three.min.js", array(),REAL3D_FLIPBOOK_VERSION);
	     }
	     if (!wp_script_is( 'real3d_flipbook_webgl', 'enqueued' )) {
	     	wp_enqueue_script("real3d_flipbook_webgl", plugins_url()."/real3d-flipbook/js/flipbook.webgl.min.js", array(),REAL3D_FLIPBOOK_VERSION);
	     }
     }

     if($flipbook['pdfUrl'] != -1 || $flipbook['type'] == 'pdf'){

	     if (!wp_script_is( 'real3d_flipbook_pdfjs', 'enqueued' )) {
	     	wp_enqueue_script("real3d_flipbook_pdfjs", plugins_url()."/real3d-flipbook/js/pdf.min.js", array(),REAL3D_FLIPBOOK_VERSION);
	     }

	     if (!wp_script_is( 'real3d_flipbook_pdfservice', 'enqueued' )) {
	     	wp_enqueue_script("real3d_flipbook_pdfservice", plugins_url()."/real3d-flipbook/js/flipbook.pdfservice.min.js", array(),REAL3D_FLIPBOOK_VERSION);
	     }

     }


     if (!wp_script_is( 'real3d_flipbook_embed', 'enqueued' )) {
     	wp_enqueue_script("real3d_flipbook_embed", plugins_url()."/real3d-flipbook/js/embed.js", array('real3d_flipbook'),REAL3D_FLIPBOOK_VERSION);
     }
     wp_localize_script( 'real3d_flipbook_embed', 'real3dflipbook_'.$bookId, json_encode($flipbook) );


     if (!wp_style_is( 'flipbook_style', 'enqueued' )) {
     	wp_enqueue_style( 'flipbook_style', plugins_url()."/real3d-flipbook/css/flipbook.style.css" , array(),REAL3D_FLIPBOOK_VERSION);
     }
     if (!wp_style_is( 'font_awesome', 'enqueued' )) {
     	wp_enqueue_style( 'font_awesome', plugins_url()."/real3d-flipbook/css/font-awesome.css" , array(),REAL3D_FLIPBOOK_VERSION);
     }
	return $output;
}
add_filter('widget_text', 'do_shortcode');
add_shortcode('real3dflipbook', 'real3dflipbook_shortcode');