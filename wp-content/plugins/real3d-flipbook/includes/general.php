<?php 
    if ( ! defined( 'ABSPATH' ) ) {
        exit; // Exit if accessed directly
    }
?>

<div class='wrap'>
<div id='real3dflipbook-admin' style="display:none;">
   <a href="admin.php?page=real3d_flipbook_admin" class="back-to-list-link">&larr; 
   <?php _e('Back to flipbooks list', 'flipbook'); ?>
   </a>
   <h1>Global Settings</h1>
   <p>Global default settings for all flipbooks.</p>
   <form method="post" id="real3dflipbook-options-form" enctype="multipart/form-data" action="admin.php?page=real3d_flipbook_admin&action=save_settings">
      <div>
         <h2 id="r3d-tabs" class="nav-tab-wrapper wp-clearfix">
            <a href="#" class="nav-tab" data-tab="tab-general">General</a>
            <a href="#" class="nav-tab" data-tab="tab-lightbox">Lightbox</a>
            <a href="#" class="nav-tab" data-tab="tab-menu">Menu</a>
            <a href="#" class="nav-tab" data-tab="tab-webgl">WebGL</a>
            <a href="#" class="nav-tab" data-tab="tab-mobile">Mobile</a>
            <a href="#" class="nav-tab" data-tab="tab-ui">UI</a>
            <a href="#" class="nav-tab" data-tab="tab-translate">Translate</a>
         </h2>
         </div>
         <div class="">
            <div id="tab-general" style="display:none;">
               <table class="form-table" id="flipbook-general-options">
                  <tbody></tbody>
               </table>
            </div>
            <div id="tab-normal"  style="display:none;">
               <table class="form-table" id="flipbook-normal-options">
                  <tbody></tbody>
               </table>
            </div>
            <div id="tab-mobile"  style="display:none;">
               <p class="description">
               <p>Override settings for mobile devices (use different view mode, smaller textures ect)</p>
               </p>
               <table class="form-table" id="flipbook-mobile-options">
                  <tbody></tbody>
               </table>
            </div>
            <div id="tab-lightbox"  style="display:none;">
               <table class="form-table" id="flipbook-lightbox-options">
                  <tbody></tbody>
               </table>
            </div>
            <div id="tab-webgl"  style="display:none;">
               <table class="form-table" id="flipbook-webgl-options">
                  <tbody></tbody>
               </table>
            </div>
            <div id="tab-menu"  style="display:none;">
               <h3 class="hndle">Current page</h3>
               <table class="form-table" id="flipbook-currentPage-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button autoplay</h3>
               <table class="form-table" id="flipbook-btnAutoplay-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button next page</h3>
               <table class="form-table" id="flipbook-btnNext-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button last page</h3>
               <table class="form-table" id="flipbook-btnLast-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button previous page</h3>
               <table class="form-table" id="flipbook-btnPrev-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button first page</h3>
               <table class="form-table" id="flipbook-btnFirst-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button zoom in</h3>
               <table class="form-table" id="flipbook-btnZoomIn-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button zoom out</h3>
               <table class="form-table" id="flipbook-btnZoomOut-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button table of content</h3>
               <table class="form-table" id="flipbook-btnToc-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button thumbnails</h3>
               <table class="form-table" id="flipbook-btnThumbs-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button share</h3>
               <table class="form-table" id="flipbook-btnShare-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button download pages</h3>
               <table class="form-table" id="flipbook-btnDownloadPages-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button download PDF</h3>
               <table class="form-table" id="flipbook-btnDownloadPdf-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button sound</h3>
               <table class="form-table" id="flipbook-btnSound-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button expand</h3>
               <table class="form-table" id="flipbook-btnExpand-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button select tool</h3>
               <table class="form-table" id="flipbook-btnSelect-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button search</h3>
               <table class="form-table" id="flipbook-btnSearch-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Button print</h3>
               <table class="form-table" id="flipbook-btnPrint-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Share on Google plus</h3>
               <table class="form-table" id="flipbook-google_plus-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Share on Twitter</h3>
               <table class="form-table" id="flipbook-twitter-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Share on Facebook</h3>
               <table class="form-table" id="flipbook-facebook-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Share on pinterest</h3>
               <table class="form-table" id="flipbook-pinterest-options">
                  <tbody></tbody>
               </table>
               <h3 class="hndle">Share by email</h3>
               <table class="form-table" id="flipbook-email-options">
                  <tbody></tbody>
               </table>
            </div>
            <div id="tab-ui"  style="display:none;">
               <table class="form-table" id="flipbook-theme">
                  <tbody>
                     <tr valign="top" class="field-row">
                        <th scope="row">UI theme</th>
                        <td>
                           <select name="theme">
                              <option name="theme" value="default" selected="selected">default</option>
                              <option name="theme" value="demo1">demo1</option>
                              <option name="theme" value="demo1">demo2</option>
                              <option name="theme" value="demo1">demo3</option>
                              <option name="theme" value="demo1">demo4</option>
                              <option name="theme" value="demo1">demo5</option>
                              <option name="theme" value="demo1">demo6</option>
                              <option name="theme" value="demo1">demo7</option>
                              <option name="theme" value="demo1">demo8</option>
                           </select>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <h3>Advanced settings</h3>
               <table class="form-table" id="flipbook-ui-options">
                  <tbody></tbody>
               </table>
               <h3>Background</h3>
               <table class="form-table" id="flipbook-bg-options">
                  <tbody></tbody>
               </table>
               <h3>Menu bar</h3>
               <table class="form-table" id="flipbook-menu-bar-options">
                  <tbody></tbody>
               </table>
               <h3>Menu buttons</h3>
               <table class="form-table" id="flipbook-menu-buttons-options">
                  <tbody></tbody>
               </table>
               <h3>Side navigation buttons</h3>
               <table class="form-table" id="flipbook-side-buttons-options">
                  <tbody></tbody>
               </table>
               <h3>Close button</h3>
               <table class="form-table" id="flipbook-close-button-options">
                  <tbody></tbody>
               </table>
               <h3>Current page display</h3>
               <table class="form-table" id="flipbook-current-page-options">
                  <tbody></tbody>
               </table>
            </div>
            <div id="tab-translate"  style="display:none;">
               <table class="form-table" id="flipbook-translate-options">
                  <tbody></tbody>
               </table>
            </div>
         </div>
      </div>
      <p id="r3d-save" class="submit">
         <span class="spinner"></span>
         <!-- <a class="update-all-flipbooks alignright" href='#'>Save this settings for all flipbooks</a> --> 
         <input type="submit" name="btbsubmit" id="btbsubmit" class="alignright button save-button button-primary" value="Save">
         <a href="#" class="alignright flipbook-preview button button-secondary">Preview</a>
         <a href="#" class="alignright flipbook-reset-defaults button button-secondary">Reset to defaults</a>
      </p>
      <div id="r3d-save-holder" style="display: none;" />
   </form>
   </div>
</div>
<?php 
// trace("real3dflipbook_global");
// trace(get_option("real3dflipbook_global"));
wp_enqueue_style( 'font_awesome', plugins_url(). "/real3d-flipbook/css/font-awesome.css" , array(),REAL3D_FLIPBOOK_VERSION); 
wp_enqueue_script(
'alpha-color-picker',
plugins_url(). '/real3d-flipbook/js/alpha-color-picker.js', 
array( 'jquery', 'wp-color-picker' ),
REAL3D_FLIPBOOK_VERSION,
true
);
wp_enqueue_style(
'alpha-color-picker',
plugins_url(). '/real3d-flipbook/css/alpha-color-picker.css', 
array( 'wp-color-picker' ),
REAL3D_FLIPBOOK_VERSION
);
wp_enqueue_script( "r3dfb-general", plugins_url(). "/real3d-flipbook/js/general.js", array( 'jquery', 'jquery-ui-sortable', 'jquery-ui-resizable', 'jquery-ui-selectable', 'alpha-color-picker' ),REAL3D_FLIPBOOK_VERSION); 
wp_enqueue_style( 'real3d_flipbook_admin_css', plugins_url(). "/real3d-flipbook/css/flipbook-admin.css",array(), REAL3D_FLIPBOOK_VERSION ); 
$ajax_nonce = wp_create_nonce( "r3dfb_general");
$flipbook = get_option("real3dflipbook_global");
$flipbook['security'] = $ajax_nonce;
// if($current_id != ''){
wp_localize_script( 'r3dfb-general', 'options', json_encode($flipbook) );