(function($) {

    $(document).ready(function() {

        $('#real3dflipbook-admin').show()

        $('.creating-page').hide()

        options = $.parseJSON(window.options)

        function convertStrings(obj) {

            $.each(obj, function(key, value) {
                // console.log(key + ": " + options[key]);
                if (typeof(value) == 'object' || typeof(value) == 'array') {
                    convertStrings(value)
                } else if (!isNaN(value)) {
                    if (obj[key] == "")
                        delete obj[key]
                    else
                        obj[key] = Number(value)
                } else if (value == "true") {
                    obj[key] = true
                } else if (value == "false") {
                    obj[key] = false
                }
            });

        }
        convertStrings(options)

        console.log(options);

        var title = 'General settings'

        $("#edit-flipbook-text").text(title)

        addOptionGeneral(
            "mode", 
            "dropdown",         
            "Mode", 
            '<strong>normal</strong> - embedded in a container div<br/><strong>lightbox</strong> - opens in fullscreen overlay on click<br/><strong>fullscreen</strong> - covers entire page',
            ["normal", "lightbox", "fullscreen"]
        );
        
        addOptionGeneral(
            "viewMode", 
            "dropdown",         
            "View mode", 
            '<strong>webgl</strong> - realistic 3D page flip with lights and shadows<br/><strong>3d</strong> - CSS 3D flip<br/><strong>swipe</strong> - horizontal swipe<br/><strong>simple</strong> - no animation',
            ["webgl", "3d", "2d", "swipe", "simple"]
        );
        
        addOptionGeneral( 
            "zoomMin",          
            "text", 
            "Initial zoom", 
            'Initial book zoom, recommended between 0.8 and 1'
        );
        
        addOptionGeneral( 
            "zoomStep",         
            "text", 
            "Zoom step", 
            'Between 1.1 and 4'
        );
        
        addOptionGeneral( 
            "zoomDisabled",     
            "checkbox", 
            "Disable mouse wheel zoom"
        );

        addOptionGeneral( 
            "singlePageMode",   
            "checkbox", 
            "Single page view", 
            'Display one page at a time'
        );

        addOptionGeneral( 
            "pageFlipDuration", 
            "text", 
            "Flip duration", 
            'Duration of flip animation, recommended between 0.5 and 2'
        );

        addOptionGeneral( 
            "sound",            
            "checkbox", 
            "Page flip sound"
        );
        addOptionGeneral( 
            "startPage",        
            "text", 
            "Start page", 
            'Open flipbook at this page at start'
        );

        addOptionGeneral( 
            "deeplinking[enabled]", 
            "checkbox", 
            "Deep linking", 
            'Enables to link to specific page within flipbook, for example if flipbook is at example.com, open page 2 with example.com#2'
        );

        addOptionGeneral( 
            "deeplinking[prefix]", 
            "text", 
            "Deep linking prefix", 
            'Custom deep linking prefix, for example "book1_", link to page 2 will be example.com#book1_2'
        );
        
        addOptionGeneral( 
            "responsiveView",   
            "checkbox",
            "Responsive view", 
            'Switching from two page layout to one page layout if flipbook width is below certain treshold'
        );
        
        addOptionGeneral( 
            "responsiveViewTreshold", 
            "text", 
            "Responsive view treshold", 
            'Treshold (screen width in px) for responsive view feature'
        );

        addOptionGeneral(
            "pageTextureSize",  
            "text", 
            "PDF page size", 
            'The height of rendered pdf pages in px.'
        );
        
        addOptionGeneral( 
            "textLayer", 
            "checkbox", 
            "PDF text layer", 
            'PDF text selection'
        );
        
        addOptionGeneral( 
            "pdfPageScale", 
            "text", 
            "PDF page scale"
        );
        
        addOptionGeneral( 
            "aspectRatio", 
            "text", 
            "Container responsive ratio", 
            'Container width / height ratio, recommended between 1 and 2'
        );
        
        addOptionGeneral( 
            "thumbnailsOnStart", 
            "checkbox", 
            "Show thumbnails on start"
        );
        
        addOptionGeneral( 
            "contentOnStart", 
            "checkbox", 
            "Show table of content on start"
        );
        
        addOptionGeneral( 
            "tableOfContentCloseOnClick",
             "checkbox", 
             "Close table of content when page is clicked"
        );
        
        addOptionGeneral( 
            "thumbsCloseOnClick", 
            "checkbox", 
            "Close thumbnails when page is clicked"
        );
        
        addOptionGeneral( 
            "autoplayOnStart", 
            "checkbox", 
            "Autoplay on start"
        );
        
        addOptionGeneral( 
            "autoplayInterval", 
            "text", 
            "Autoplay interval (ms)"
        );
        
        addOptionGeneral( 
            "autoplayStartPage", 
            "text", 
            "Autoplay start page"
        );
        
        addOptionGeneral( 
            "rightToLeft", 
            "checkbox", 
            "Right to left mode", 
            'Flipping from right to left (inverted)'
        );
        
        addOptionGeneral( 
            "thumbSize",
            "text", 
            "Thumbnail size", 
            'Thumbnail height for thumbnails view'
        );

        addOptionGeneral( 
            "logoImg",
            "selectImage", 
            "Logo image",
            'Logo image that will be displayed inside the flipbook container'
        );

        addOptionGeneral( 
            "logoUrl",
            "text", 
            "Logo link", 
            'URL that will be opened on logo click'
        );

        addOptionGeneral( 
            "logoCSS", 
            "textarea",
            "Logo CSS",
            'Custom CSS for logo'
        );

        addOptionGeneral( 
            "menuSelector",
            "text",
            "Menu css selector",
            'Example "#menu" or ".navbar". Used with mode "fullscreen" so the flipbook will be resized correctly below the menu'
        );

        addOptionGeneral( 
            "zIndex", 
            "text", 
            "Container z-index",
            'Set z-index of flipbook container'
        );

        addOptionGeneral( 
            'preloaderText', 
            'text', 
            'Preloader text', 
            'Text that will be displayed under the preloader spinner'
        );

        addOptionGeneral( 
            'googleAnalyticsTrackingCode', 
            'text', 
            'Google analytics tracking code'
        );

        addOptionGeneral( 
            "pdfBrowserViewerIfIE",         
            "checkbox",         
            "Download PDF instead of displaying flipbook if browser is Internet Explorer", 
            'For pdf flipbook'
        );

        addOptionMobile( 
            "viewModeMobile", "dropdown",         
            "View mode", 
            'Override default view mode for mobile',
            ["", "webgl", "3d", "2d", "swipe", "simple"]
        );

        addOptionMobile( 
            "pageTextureSizeMobile",  
            "text", 
            "PDF page size", 
            'The height of rendered pdf pages in px.'
        );

        addOptionMobile( 
            "aspectRatioMobile", 
            "text", 
            "Container responsive ratio", 
            'Container width / height ratio, recommended between 1 and 2'
        );

        addOptionMobile( 
            "singlePageModeIfMobile", 
            "checkbox", 
            "Single page view", 
            'Display one page at a time'
        );

        addOptionMobile( 
            "pdfBrowserViewerIfMobile",
            "checkbox", 
            "Use default device pdf viewer instead of flipbook", 
            'Opens pdf file directly in browser, instead of flipbook'
        );

        addOptionMobile(
         "pdfBrowserViewerFullscreen", 
         "checkbox", 
         "Default device pdf viewer fullscreen"
        );

        addOptionMobile( 
            "pdfBrowserViewerFullscreenTarget",
            "dropdown", 
            "Default device pdf viewer target", 
            'Opens pdf file in new tab or in same tab',
            ["_self", "_blank"]
        );

        addOptionMobile( 
            "btnTocIfMobile", 
            "checkbox", 
            "Button table of content"
        );

        addOptionMobile( 
            "btnThumbsIfMobile", 
            "checkbox", 
            "Button thumbnails"
        );

        addOptionMobile( 
            "btnShareIfMobile", 
            "checkbox", 
            "Button share"
        );

        addOptionMobile( 
            "btnDownloadPagesIfMobile", 
            "checkbox", 
            "Button download pages"
        );

        addOptionMobile( 
            "btnDownloadPdfIfMobile", 
            "checkbox", 
            "Button view pdf"
        );

        addOptionMobile( 
            "btnSoundIfMobile", 
            "checkbox", 
            "Button sound"
        );

        addOptionMobile( 
            "btnExpandIfMobile", 
            "checkbox", 
            "Button fullscreen"
        );

        addOptionMobile( 
            "btnPrintIfMobile", 
            "checkbox", 
            "Button print"
        );

        addOptionMobile( 
            "logoHideOnMobile",
             "checkbox", 
             "Hide logo"
        );

        addOptionLightbox( 
            "lightboxCSS", 
            "textarea", 
            "Lightbox overlay CSS", 
            'Custom CSS for ligtbox overlay element'
        );

        addOptionLightbox( 
            "lightboxBackground", 
            "text", 
            "Lightbox overlay background", 
            'Lightbox background CSS, for example hex color #999 or rgb color rgb(128,128,128) or rgba color with transparency rgba(0,0,0,0.5) or image url("images/background.jpg")'
        );

        addOptionLightbox( 
            "lightboxContainerCSS", 
            "textarea", 
            "Thumbnail container CSS"
        );
        
        addOptionLightbox( 
            "lightboxThumbnailHeight", 
            "text", 
            "Thumbnail height", 
            'Height of thumbnail that will be generated from PDF'
        );

        addOptionLightbox( 
            "lightboxThumbnailUrlCSS", 
            "textarea", 
            "Thumbnail CSS",  
            'Custom CSS for lightbox thumbnail image'
        );

        addOptionLightbox( 
            "lightboxText", 
            "text", 
            "Text link", 
            'Text that will be displayed in place of shortcode'
        );

        addOptionLightbox( 
            "lightboxTextCSS", 
            "textarea", 
            "Text link CSS",
            'Custom CSS for text link'
        );

        addOptionLightbox( 
            "lightboxTextPosition", 
            "dropdown", 
            "Text link position", 
            'Text link above or below the thumbnail',
            ["top", "bottom"]
        );

        addOptionLightbox( 
            "lightBoxOpened", 
            "checkbox", 
            "Opened on start", 
            'Lightbox will open automatically on page load'
        );

        addOptionLightbox( 
            "lightBoxFullscreen", 
            "checkbox", 
            "Openes in fullscreen", 
            'Opening the lightbox will put lightbox element to real fullscreen'
        );

        addOptionLightbox( 
            "lightboxCloseOnClick", 
            "checkbox", 
            "Closes when clicked outside the book", 
            'Close lightbox if clicked on the overlay but outside the book'
        );

        addOptionLightbox( 
            "lightboxMarginV", 
            "text", 
            "Vertical margin",
            'Lightbox overlay vertical margin'
        );

        addOptionLightbox   ( 
            "lightboxMarginH", 
            "text", 
            "Horizontal margin",
            'Lightbox overlay horizontal margin'
        );

        addOption(
            "currentPage", 
            "currentPage[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "currentPage", 
            "currentPage[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnAutoplay", 
            "btnAutoplay[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnAutoplay", 
            "btnAutoplay[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnAutoplay", 
            "btnAutoplay[iconAlt]", 
            "text", 
            "Icon alt CSS class"
        );

        addOption(
            "btnAutoplay", 
            "btnAutoplay[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnNext", 
            "btnNext[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnNext", 
            "btnNext[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnNext", 
            "btnNext[title]", 
            "text", 
            "Title",
        );

        addOption(
            "btnFirst", 
            "btnFirst[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnFirst", 
            "btnFirst[icon]", 
            "text", 
            "Button font awesome CSS class"
        );

        addOption(
            "btnFirst", 
            "btnFirst[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnLast", 
            "btnLast[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnLast", 
            "btnLast[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnLast", 
            "btnLast[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnPrev", 
            "btnPrev[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnPrev", 
            "btnPrev[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnPrev", 
            "btnPrev[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnZoomIn", 
            "btnZoomIn[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnZoomIn", 
            "btnZoomIn[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnZoomIn", 
            "btnZoomIn[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnZoomOut", 
            "btnZoomOut[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnZoomOut", 
            "btnZoomOut[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnZoomOut", 
            "btnZoomOut[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnToc", 
            "btnToc[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnToc", 
            "btnToc[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnToc", 
            "btnToc[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnThumbs", 
            "btnThumbs[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnThumbs", 
            "btnThumbs[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnThumbs", 
            "btnThumbs[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnShare", 
            "btnShare[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnShare", 
            "btnShare[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnShare", 
            "btnShare[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnSound", 
            "btnSound[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnSound", 
            "btnSound[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnSound", 
            "btnSound[iconAlt]", 
            "text", 
            "Alt Icon CSS class"
        );

        addOption(
            "btnSound", 
            "btnSound[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnDownloadPages", 
            "btnDownloadPages[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnDownloadPages", 
            "btnDownloadPages[url]", 
            "selectFile", 
            "Url of zip file containing all pages"
        );

        addOption(
            "btnDownloadPages", 
            "btnDownloadPages[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnDownloadPages", 
            "btnDownloadPages[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnDownloadPdf", 
            "btnDownloadPdf[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnDownloadPdf", 
            "btnDownloadPdf[url]", 
            "selectFile", 
            "PDF file url"
        );

        addOption(
            "btnDownloadPdf", 
            "btnDownloadPdf[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnDownloadPdf", 
            "btnDownloadPdf[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnDownloadPdf", 
            "btnDownloadPdf[forceDownload]", 
            "checkbox", 
            "Force download"
        );

        addOption(
            "btnDownloadPdf", 
            "btnDownloadPdf[openInNewWindow]", 
            "checkbox", 
            "Open PDF in new browser window"
        );

        addOption(
            "btnPrint", 
            "btnPrint[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnPrint", 
            "printPdfUrl", 
            "selectFile", 
            "PDF file for printing"
        );

        addOption(
            "btnPrint", 
            "btnPrint[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnPrint", 
            "btnPrint[title]", 
            "text", 
            "Button title"
        );

        addOption(
            "btnExpand", 
            "btnExpand[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnExpand", 
            "btnExpand[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnExpand", 
            "btnExpand[iconAlt]", 
            "text", 
            "Icon CSS class alt"
        );

        addOption(
            "btnExpand", 
            "btnExpand[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnSelect", 
            "btnSelect[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnSelect", 
            "btnSelect[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnSelect", 
            "btnSelect[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnSearch", 
            "btnSearch[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnSearch", 
            "btnSearch[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnSearch", 
            "btnSearch[title]", 
            "text", 
            "Title"
        );

        addOption(
            "google_plus", 
            "google_plus[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "google_plus", 
            "google_plus[url]", 
            "text", 
            "URL"
        );

        addOption(
            "twitter", 
            "twitter[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "twitter", 
            "twitter[url]", 
            "text", 
            "URL"
        );

        addOption(
            "twitter", 
            "twitter[description]", 
            "text", 
            "Description"
        );

        addOption(
            "facebook", 
            "facebook[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "facebook", 
            "facebook[url]", 
            "text", 
            "URL"
        );

        addOption(
            "facebook", 
            "facebook[description]", 
            "text", 
            "Description"
        );

        addOption(
            "facebook", 
            "facebook[title]", 
            "text", 
            "Title"
        );

        addOption(
            "facebook", 
            "facebook[image]", 
            "text", 
            "Image"
        );

        addOption(
            "facebook", 
            "facebook[caption]", 
            "text", 
            "Caption"
        );

        addOption(
            "pinterest", 
            "pinterest[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "pinterest", 
            "pinterest[url]", 
            "text", 
            "URL"
        );

        addOption(
            "pinterest", 
            "pinterest[image]", 
            "text", 
            "Image"
        );

        addOption(
            "pinterest", 
            "pinterest[description]", 
            "text", 
            "Description"
        );

        addOption(
            "email", 
            "email[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "email", 
            "email[url]", 
            "text", 
            "URL"
        );

        addOption(
            "email", 
            "email[description]", 
            "text", 
            "Description"
        );

        addOptionWebgl(
            "lights", 
            "checkbox", 
            "Lights",
            'Realistic lightning, disable for faster performance'
        );

        // addOptionWebgl(
        //     "lightColor", 
        //     "text", 
        //     "Light color", 
        //     "0xFFFFFF"
        // );

        addOptionWebgl(
            "lightPositionX", 
            "text", 
            "Light pposition x", 
            'Light X position, between -500 and 500'
        );

        addOptionWebgl(
            "lightPositionY", 
            "text", 
            "Light position y", 
            'Light Y position, between -500 and 500'
        );

        addOptionWebgl(
            "lightPositionZ", 
            "text", 
            "Light position z", 
            'Light Z position, between 1000 and 2000'
        );

        addOptionWebgl(
            "lightIntensity", 
            "text", 
            "Light intensity", 
            'Light intensity, between 0 and 1'
        );

        addOptionWebgl(
            "shadows", 
            "checkbox", 
            "Shadows", 
            'Realistic page shadows, disable for faster performance'
        );

        addOptionWebgl(
            "shadowMapSize", 
            "text", 
            "Shadow Map Size", 
            'Shadow quality, 1024 or 2048 or 4096'
        );

        addOptionWebgl(
            "shadowOpacity", 
            "text", 
            "Shadow opacity", 
            'Shadow darkness, between 0 and 1'
        );

        addOptionWebgl(
            "shadowDistance", 
            "text", 
            "Shadow plane distance", 
            'Distance of shadow from the page, between 10 and 20'
        );

        addOptionWebgl(
            "pageHardness", 
            "text", 
            "Page hardness", 
            'Between 1 and 5'
        );

        addOptionWebgl(
            "coverHardness", 
            "text", 
            "Cover hardness", 
            'Between 1 and 5'
        );

        addOptionWebgl(
            "pageRoughness", 
            "text", 
            "Page material roughness", 
            'Between 0 and 1'
        );

        addOptionWebgl(
            "pageMetalness", 
            "text", 
            "Page material metalness", 
            'Between 0 and 1'
        );

        addOptionWebgl(
            "pageSegmentsW", 
            "text", 
            "Page segments W", 

            'Number of page segments horizontally'
        );

        addOptionWebgl(
            "pageSegmentsH", 
            "text", 
            "Page segments H", 
            'Number of page polygons vertically'
        );

        addOptionWebgl(
            "pageMiddleShadowSize", 
            "text", 
            "Page middle shadow size", 
            'Shadow in the middle of the book'
        );

        addOptionWebgl(
            "pageMiddleShadowColorL", 
            "color", 
            "Left middle shadow color"
        );

        addOptionWebgl(
            "pageMiddleShadowColorR", 
            "color", 
            "Right middle shadow color"
        );

        addOptionWebgl(
            "antialias", 
            "checkbox", 
            "Antialiasing", 
            'Disable for faster performance'
        );

        addOptionWebgl(
            "pan", 
            "text", 
            "Camera pan angle", 
            'Between -10 and 10'
        );

        addOptionWebgl(
            "tilt", 
            "text", 
            "Camera tilt angle", 
            'Between -30 and 0'
        );

        addOptionWebgl(
            "rotateCameraOnMouseDrag", 
            "checkbox", 
            "Rotate camera on mouse drag"
        );

        addOptionWebgl(
            "panMax", 
            "text", 
            "Camera pan max", 
            'Max pan angle for mouse drag rotate, between 0 and 20'
        );

        addOptionWebgl(
            "panMin", 
            "text", 
            "Camera pan min", 
            'Min pan angle for mouse drag rotate, between -20 and 0'
        );

        addOptionWebgl(
            "tiltMax", 
            "text", 
            "Camera tilt max", 
            'Max tilt angle for mouse drag rotate, between -60 and 0'
        );

        addOptionWebgl(
            "tiltMin", 
            "text", 
            "Camera tilt min", 
            'Min tilt angle for mouse drag rotate, between -60 and 0'
        );

        //UI
        addOption(
            "menu-bar", 
            "menuBackground", 
            "color", 
            "Background color"
        );

        addOption(
            "menu-bar", 
            "menuShadow", 
            "text", 
            "Shadow", 
            'Custom CSS'
        );

        addOption(
            "menu-bar", 
            "menuMargin", 
            "text", 
            "Margin"
        );

        addOption(
            "menu-bar", 
            "menuPadding", 
            "text", 
            "Padding"
        );

        addOption(
            "menu-bar", 
            "menuOverBook", 
            "checkbox", 
            "Menu over book", 
            'Menu on top of book (overlay)'
        );

        addOption(
            "menu-bar", 
            "hideMenu", 
            "checkbox", 
            "Hide menu", 
            'Hide menu completely'
        );

        addOption(
            "menu-buttons", 
            "menuAlignHorizontal", 
            "dropdown", 
            "Horizontal position", 
            "",
            ['right', 'left', 'center']
        );

        addOption(
            "menu-buttons", 
            "btnColor", 
            "color", 
            "Color"
        );

        addOption(
            "menu-buttons", 
            "btnBackground", 
            "color", 
            "Background color"
        );

        addOption(
            "menu-buttons", 
            "btnRadius", 
            "text", 
            "Radius", 
            'px'
        );

        addOption(
            "menu-buttons", 
            "btnMargin", 
            "text", 
            "Margin", 
            'px'
        );

        addOption(
            "menu-buttons", 
            "btnSize", 
            "text", 
            "Size", 
            'Size of menu buttons, between 8 and 20'
        );

        addOption(
            "menu-buttons", 
            "btnPaddingV", 
            "text", 
            "Padding vertical", 
            'Padding vertical of menu buttons, between 0 and 20'
        );

        addOption(
            "menu-buttons", 
            "btnPaddingH", 
            "text", 
            "Padding horizontal", 
            'Padding horizontal of menu buttons, between 0 and 20'
        );

        addOption(
            "menu-buttons", 
            "btnShadow", 
            "text", 
            "Box shadow", 
            'CSS value'
        );

        addOption(
            "menu-buttons", 
            "btnTextShadow", 
            "text", 
            "Text shadow", 
            'CSS value'
        );

        addOption(
            "menu-buttons", 
            "btnBorder", 
            "text", 
            "Border", 
            'CSS value'
        );

        addOption(
            "side-buttons", 
            "sideBtnColor", 
            "color", 
            "Color"
        );

        addOption(
            "side-buttons", 
            "sideBtnBackground", 
            "color", 
            "Background color"
        );

        addOption(
            "side-buttons", 
            "sideBtnRadius", 
            "text", 
            "Radius", 
            'px'
        );

        addOption(
            "side-buttons", 
            "sideBtnMargin", 
            "text", 
            "Margin", 
            'px'
        );

        addOption(
            "side-buttons", 
            "sideBtnSize", 
            "text", 
            "Size", 
            'Side buttons margin size, between 8 and 50'
        );

        addOption(
            "side-buttons", 
            "sideBtnPaddingV", 
            "text", 
            "Padding vertical", 
            'Side buttons padding vertical, between 0 and 10'
        );

        addOption(
            "side-buttons", 
            "sideBtnPaddingH", 
            "text", 
            "Padding horizontal", 
            'Side buttons padding horizontal, between 0 and 10'
        );

        addOption(
            "side-buttons", 
            "sideBtnShadow", 
            "text", 
            "Box shadow", 
            'CSS value'
        );

        addOption(
            "side-buttons", 
            "sideBtnTextShadow", 
            "text", 
            "Text shadow", 
            'CSS value'
        );

        addOption(
            "side-buttons", 
            "sideBtnBorder", 
            "text", 
            "Border", 
            'CSS value'
        );

        addOption(
            "current-page", 
            "currentPagePositionV", 
            "dropdown", 
            'Current page display vertical position',
            "Vertical position", ["top", "bottom"]
        );

        addOption(
            "current-page", 
            "currentPagePositionH", 
            "dropdown", 
            "Horizontal position",
            'Current page display horizontal position',
            ["left", "right"]
        );

        addOption(
            "current-page", 
            "currentPageMarginV", 
            "text", 
            "Vertical margin", 
            'Between 0 and 10'
        );

        addOption(
            "current-page", 
            "currentPageMarginH", 
            "text", 
            "Horizontal margin", 
            'Between 0 and 10'
        );

        addOption(
            "close-button", 
            "closeBtnColor", 
            "color", 
            "Color"
        );

        addOption(
            "close-button", 
            "closeBtnBackground", 
            "color", 
            "Background color"
        );

        addOption(
            "close-button", 
            "closeBtnRadius", 
            "text", 
            "Radius", 
            'CSS value'
        );

        addOption(
            "close-button", 
            "closeBtnMargin", 
            "text", 
            "Margin", 

            'CSS value'
        );

        addOption(
            "close-button", 
            "closeBtnSize", 
            "text", 
            "Size", 
            'Between 8 and 30'
        );

        addOption(
            "close-button", 
            "closeBtnPadding", 
            "text", 
            "Padding", 
            'Between 0 and 10'
        );

        addOption(
            "close-button", 
            "closeBtnTextShadow", 
            "text", 
            "Text shadow", 
            'CSS value'
        );

        addOption(
            "close-button", 
            "closeBtnBorder", 
            "text", 
            "Border", 
            'CSS value'
        );

        addOption(
            "ui", 
            "skin", 
            "dropdown", 
            "Skin", 
            '',
            ["light", "dark", 'lightgrey', 'darkgrey']
        );

        addOption(
            "ui", 
            "sideNavigationButtons", 
            "checkbox", 
            "Side navigation buttons", 
            'Show navigation buttons on the sides'
        );

        addOption(
            "bg", 
            "backgroundColor", 
            "color", 
            "Background color"
        );

        addOption(
            "bg", 
            "backgroundPattern", 
            "selectImage", 
            "Background image (repeated)", 
            'Flipbook container background image that is repeated'
        );

        addOption(
            "bg", 
            "backgroundTransparent", 
            "checkbox", 
            "Transparent", 
            'Flipbook container will have transparent background'
        );

        //translate

        addOption(
            "translate", 
            "strings[print]", 
            "text", 
            "Print"
        );

        addOption(
            "translate", 
            "strings[printLeftPage]", 
            "text", 
            "Print left page"
        );

        addOption(
            "translate", 
            "strings[printRightPage]", 
            "text", 
            "Print right page"
        );

        addOption(
            "translate", 
            "strings[printCurrentPage]", 
            "text", 
            "Print current page"
        );

        addOption(
            "translate", 
            "strings[printAllPages]", 
            "text", 
            "Print all pages"
        );

        addOption(
            "translate", 
            "strings[download]", 
            "text", 
            "Download"
        );

        addOption(
            "translate", 
            "strings[downloadLeftPage]", 
            "text", 
            "Download left page"
        );

        addOption(
            "translate", 
            "strings[downloadRightPage]", 
            "text", 
            "Download right page"
        );

        addOption(
            "translate", 
            "strings[downloadCurrentPage]", 
            "text", 
            "Download current page"
        );

        addOption(
            "translate", 
            "strings[downloadAllPages]", 
            "text", 
            "Download all pages"
        );

        addOption(
            "translate", 
            "strings[bookmarks]", 
            "text", 
            "Bookmarks"
        );

        addOption(
            "translate", 
            "strings[bookmarkLeftPage]", 
            "text", 
            "Bookmark left page"
        );

        addOption(
            "translate", 
            "strings[bookmarkRightPage]", 
            "text", 
            "Bookmark right page"
        );

        addOption(
            "translate", 
            "strings[bookmarkCurrentPage]", 
            "text", 
            "Bookmark current page"
        );

        addOption(
            "translate", 
            "strings[search]", 
            "text", 
            "Search"
        );

        addOption(
            "translate", 
            "strings[findInDocument]", 
            "text", 
            "Find in document"
        );

        addOption(
            "translate", 
            "strings[pagesFoundContaining]", 
            "text", 
            "pages found containing"
        );

        addOption(
            "translate", 
            "strings[thumbnails]", 
            "text", 
            "Thumbnails"
        );

        addOption(
            "translate", 
            "strings[tableOfContent]", 
            "text", 
            "Table of content"
        );

        addOption(
            "translate", 
            "strings[share]", 
            "text", 
            "Share"
        );

        addOption(
            "translate", 
            "strings[pressEscToClose]", 
            "text", 
            "Press ESC to close"
        );

        $('input.alpha-color-picker').alphaColorPicker()


        var ui_themes = { "default": { "skin": "light", "sideNavigationButtons": true, "menuMargin": 0, "menuPadding": 0, "menuAlignHorizontal": "center", "menuShadow": "0 0 6px rgba(0,0,0,0.16), 0 0 6px rgba(0,0,0,0.23)", "menuBackground": "", "menuOverBook": false, "btnSize": 12, "btnRadius": 4, "btnMargin": 4, "btnPaddingV": 10, "btnPaddingH": 10, "btnShadow": "", "btnTextShadow": "", "btnBorder": "", "btnColor": "", "btnBackground": "none", "sideBtnSize": 30, "sideBtnRadius": 0, "sideBtnMargin": 0, "sideBtnPaddingV": 5, "sideBtnPaddingH": 5, "sideBtnShadow": "", "sideBtnTextShadow": "", "sideBtnBorder": "", "sideBtnColor": "#FFF", "sideBtnBackground": "rgba(0,0,0,.3)", "currentPagePositionV": "top", "currentPagePositionH": "left", "currentPageMarginV": 5, "currentPageMarginH": 5, "closeBtnSize": 20, "closeBtnRadius": 0, "closeBtnMargin": 0, "closeBtnPadding": 10, "closeBtnTextShadow": "", "closeBtnColor": "#fff", "closeBtnBackground": "rgba(0,0,0,.3)", "closeBtnBorder": "" }, "demo1": { "skin": "light", "sideNavigationButtons": "true", "menuMargin": "0", "menuPadding": "0", "menuAlignHorizontal": "center", "menuShadow": "0 0 6px rgba(0,0,0,0.16), 0 0 6px rgba(0,0,0,0.23)", "menuBackground": "", "menuOverBook": "false", "btnSize": "16", "btnRadius": "0", "btnMargin": "0", "btnPaddingV": "10", "btnPaddingH": "10", "btnShadow": "", "btnTextShadow": "", "btnBorder": "", "btnColor": "", "btnBackground": "none", "sideBtnSize": "40", "sideBtnRadius": "0", "sideBtnMargin": "5", "sideBtnPaddingV": "10", "sideBtnPaddingH": "10", "sideBtnShadow": "", "sideBtnTextShadow": "", "sideBtnBorder": "none", "sideBtnColor": "#dd4040", "sideBtnBackground": "none", "currentPagePositionV": "top", "currentPagePositionH": "left", "currentPageMarginV": "5", "currentPageMarginH": "5" }, "demo2": { "skin": "light", "sideNavigationButtons": "true", "menuMargin": "0", "menuPadding": "0", "menuAlignHorizontal": "center", "menuShadow": "none", "menuBackground": "none", "menuOverBook": "true", "btnSize": "14", "btnRadius": "4", "btnMargin": "4", "btnPaddingV": "10", "btnPaddingH": "10", "btnShadow": "", "btnTextShadow": "", "btnBorder": "", "btnColor": "#ffffff", "btnBackground": "rgba(0,0,0,.2)", "sideBtnSize": "30", "sideBtnRadius": "0", "sideBtnMargin": "0", "sideBtnPaddingV": "5", "sideBtnPaddingH": "5", "sideBtnShadow": "", "sideBtnTextShadow": "", "sideBtnBorder": "", "sideBtnColor": "#898585", "sideBtnBackground": "none", "currentPagePositionV": "top", "currentPagePositionH": "left", "currentPageMarginV": "5", "currentPageMarginH": "5" }, "demo3": { "skin": "light", "sideNavigationButtons": "false", "menuMargin": "0", "menuPadding": "5px", "menuAlignHorizontal": "right", "menuShadow": "none", "menuBackground": "none", "menuOverBook": "true", "btnSize": "12", "btnRadius": "0", "btnMargin": "0", "btnPaddingV": "12", "btnPaddingH": "12", "btnShadow": "", "btnTextShadow": "0 0 2px rgba(0,0,0,.5)", "btnBorder": "", "btnColor": "#ffffff", "btnBackground": "rgba(0,0,0,.3)", "sideBtnSize": "60", "sideBtnRadius": "50", "sideBtnMargin": "0", "sideBtnPaddingV": "5", "sideBtnPaddingH": "5", "sideBtnShadow": "", "sideBtnTextShadow": "", "sideBtnBorder": "", "sideBtnColor": "#adadad", "sideBtnBackground": "none", "currentPagePositionV": "top", "currentPagePositionH": "left", "currentPageMarginV": "5", "currentPageMarginH": "5" }, "demo4": { "skin": "light", "sideNavigationButtons": "true", "menuMargin": "0", "menuPadding": "0", "menuAlignHorizontal": "center", "menuShadow": "0 0 6px rgba(0,0,0,0.16), 0 0 6px rgba(0,0,0,0.23)", "menuBackground": "", "menuOverBook": "false", "btnSize": "12", "btnRadius": "0", "btnMargin": "0", "btnPaddingV": "10", "btnPaddingH": "10", "btnShadow": "", "btnTextShadow": "", "btnBorder": "", "btnColor": "", "btnBackground": "none", "sideBtnSize": "40", "sideBtnRadius": "0", "sideBtnMargin": "0", "sideBtnPaddingV": "", "sideBtnPaddingH": "", "sideBtnShadow": "", "sideBtnTextShadow": "", "sideBtnBorder": "", "sideBtnColor": "#000", "sideBtnBackground": "rgba(255,255,255,.2)", "currentPagePositionV": "top", "currentPagePositionH": "left", "currentPageMarginV": "5", "currentPageMarginH": "5" }, "demo5": { "skin": "dark", "sideNavigationButtons": "true", "menuMargin": "0", "menuPadding": "10", "menuAlignHorizontal": "center", "menuShadow": "none", "menuBackground": "linear-gradient(to top,rgba(0,0,0,0.65) 0%,transparent 100%)", "menuOverBook": "true", "btnSize": "14", "btnRadius": "40", "btnMargin": "5", "btnPaddingV": "10", "btnPaddingH": "10", "btnShadow": "", "btnTextShadow": "", "btnBorder": "2px solid #f4be0e", "btnColor": "#f4be0e", "btnBackground": "none", "sideBtnSize": "30", "sideBtnRadius": "80", "sideBtnMargin": "5", "sideBtnPaddingV": "10", "sideBtnPaddingH": "10", "sideBtnShadow": "0 0 2px #000", "sideBtnTextShadow": "0 0 2px #000", "sideBtnBorder": "2px solid #f4be0e", "sideBtnColor": "#f4be0e", "sideBtnBackground": "none", "currentPagePositionV": "top", "currentPagePositionH": "left", "currentPageMarginV": "5", "currentPageMarginH": "5" }, "demo6": { "skin": "dark", "sideNavigationButtons": "true", "menuMargin": "0", "menuPadding": "10", "menuAlignHorizontal": "center", "menuShadow": "none", "menuBackground": "linear-gradient(to top,rgba(0,0,0,0.65) 0%,transparent 100%)", "menuOverBook": "false", "btnSize": "16", "btnRadius": "40", "btnMargin": "5", "btnPaddingV": "12", "btnPaddingH": "12", "btnShadow": "", "btnTextShadow": "", "btnBorder": "2px solid #f4be0e", "btnColor": "#f4be0e", "btnBackground": "none", "sideBtnSize": "50", "sideBtnRadius": "80", "sideBtnMargin": "5", "sideBtnPaddingV": "", "sideBtnPaddingH": "", "sideBtnShadow": "0 0 2px #000", "sideBtnTextShadow": "0 0 2px #000", "sideBtnBorder": "2px solid #f4be0e", "sideBtnColor": "#f4be0e", "sideBtnBackground": "none" }, "demo7": { "skin": "light", "sideNavigationButtons": "true", "menuMargin": "0", "menuPadding": "15", "menuAlignHorizontal": "center", "menuShadow": "none", "menuBackground": "none", "menuOverBook": "true", "btnSize": "16", "btnRadius": "4", "btnMargin": "2", "btnPaddingV": "10", "btnPaddingH": "10", "btnShadow": "", "btnTextShadow": "", "btnBorder": "", "btnColor": "#212121", "btnBackground": "rgba(255,255,255,0.7)", "sideBtnSize": "40", "sideBtnRadius": "0", "sideBtnMargin": "0", "sideBtnPaddingV": "5", "sideBtnPaddingH": "5", "sideBtnShadow": "", "sideBtnTextShadow": "", "sideBtnBorder": "", "sideBtnColor": "#898585", "sideBtnBackground": "none", "currentPagePositionV": "top", "currentPagePositionH": "left", "currentPageMarginV": "5", "currentPageMarginH": "5" }, "demo8": { "skin": "light", "sideNavigationButtons": "true", "menuMargin": "5", "menuPadding": "0", "menuAlignHorizontal": "right", "menuShadow": "none", "menuBackground": "none", "menuOverBook": "true", "btnSize": "12", "btnRadius": "4", "btnMargin": "2", "btnPaddingV": "10", "btnPaddingH": "10", "btnShadow": "", "btnTextShadow": "", "btnBorder": "", "btnColor": "#ffffff", "btnBackground": "rgba(0,0,0,.3)", "sideBtnSize": "30", "sideBtnRadius": "4", "sideBtnMargin": "0", "sideBtnPaddingV": "", "sideBtnPaddingH": "", "sideBtnShadow": "", "sideBtnTextShadow": "", "sideBtnBorder": "", "sideBtnColor": "#ffffff", "sideBtnBackground": "rgba(0,0,0,.3)", "currentPagePositionV": "bottom", "currentPagePositionH": "left", "currentPageMarginV": "5", "currentPageMarginH": "5" } }

        $("#flipbook-theme").change(function(e) {

            var name = $("#flipbook-theme").find(":selected").text();
            var obj = ui_themes[name]
            for (var key in obj) {
                setOptionValue(key, obj[key])
            }
        })

        function updateSaveBar() {

            if ((window.innerHeight + window.scrollY) >= (document.body.scrollHeight - 50)) {

                $("#r3d-save").removeClass("r3d-save-sticky")
                $("#r3d-save-holder").hide()

            } else {

                $("#r3d-save").addClass("r3d-save-sticky")
                $("#r3d-save-holder").show()

            }

        }

        $('#real3dflipbook-admin .nav-tab').click(function(e) {
            e.preventDefault()
            $('#real3dflipbook-admin .tab-active').hide()
            $('.nav-tab-active').removeClass('nav-tab-active')
            var a = jQuery(this).addClass('nav-tab-active')
            var id = "#" + a.attr('data-tab')
            jQuery(id).addClass('tab-active').fadeIn()

            updateSaveBar()

        })

        $('#real3dflipbook-admin .nav-tab').focus(function(e) {

            this.blur()
        })

        $($('#real3dflipbook-admin .nav-tab')[0]).trigger('click')

        var $form = $('#real3dflipbook-options-form')

        // $form.submit(function(e) {

        //     e.preventDefault();

        //     // sortOptions()

        //     var $form = $(this)

        //     // console.log($form.serializeArray())

        //     $form.find('.spinner').css('visibility', 'visible')

        //     $form.find('.save-button').prop('disabled', 'disabled').css('pointer-events', 'none')

        //     var data = $form.serialize() + '&action=r3dfb_save_general&security=' + options.security

        //     $.ajax({

        //         type: "POST",
        //         url: $form.attr('action'), //.replace('admin-ajax','admin'),
        //         data: data,

        //         success: function(data, textStatus, jqXHR) {

        //             $form.find('.spinner').css('visibility', 'hidden')
        //             $form.find('.save-button').prop('disabled', '').css('pointer-events', 'auto')

        //         },
        //         error: function(XMLHttpRequest, textStatus, errorThrown) {
        //             alert("Status: " + textStatus);
        //             alert("Error: " + errorThrown);
        //         }
        //     })

        // })

          $('.flipbook-reset-defaults').click(function(e){
            e.preventDefault()
            if(confirm("Reset Global settings?"))
                location.href = "admin.php?page=real3d_flipbook_admin&action=reset_settings"
          })

        $(window).scroll(function() {
            updateSaveBar()
        })

        $(window).resize(function() {
            updateSaveBar()
        })

        updateSaveBar()

        function unsaved() { // $('.unsaved').show()
        }

        



        // flipbook-options

        if (options.socialShare == null)
            options.socialShare = [];

        for (var i = 0; i < options.socialShare.length; i++) {
            var share = options.socialShare[i];
            var shareContainer = $("#share-container");
            var shareItem = createShareHtml(i, share.name, share.icon, share.url, share.target);
            shareItem.appendTo(shareContainer);

        }

        // $(".tabs").tabs();
        $(".ui-sortable").sortable();

        $('#add-share-button').click(function(e) {

            e.preventDefault()

            var shareContainer = $("#share-container");
            var shareCount = shareContainer.find(".share").length;
            var shareItem = createShareHtml("socialShare[" + shareCount + "]", "", "", "", "", "_blank");
            shareItem.appendTo(shareContainer);

        });

       function addOptionGeneral(name, type, desc, help, values){
            addOption('general',name, type, desc, help, values)
        }

        function addOptionMobile(name, type, desc, help, values){
            addOption('mobile',name, type, desc, help, values)
        }

        function addOptionLightbox(name, type, desc, help, values){
            addOption('lightbox',name, type, desc, help, values)
        }

        function addOptionWebgl(name, type, desc, help, values){
            addOption('webgl',name, type, desc, help, values)
        }

        function addOption(section, name, type, desc, help, values){

            var val = options[name]
            if (options[name.split("[")[0]] && name.indexOf("[") != -1 && typeof(options[name.split("[")[0]]) != 'undefined') {
                 val = options[name.split("[")[0]][name.split("[")[1].split("]")[0]]
            } 

            //val = val || defaultValue
            if(typeof val == 'strings')
                val = r3d_stripslashes(val)

            var table = $("#flipbook-" + section + "-options");
            var tableBody = table.find('tbody');
            var row = $('<tr valign="top"  class="field-row"></tr>').appendTo(tableBody);
            var th = $('<th scope="row">' + desc + '</th>').appendTo(row);
            var td = $('<td></td>').appendTo(row);
            var elem

            switch (type) {

                case "text":
                    elem = $('<input type="text" name="' + name + '"/>').appendTo(td);
                    if(typeof val != 'undefined')
                        elem.attr('value', val);
                    break;

                case "color":
                    elem = $('<input type="text" name="' + name + '" class="alpha-color-picker"/>').appendTo(td);
                    elem.attr('value', val);
                    break;

                case "textarea":
                    elem = $('<textarea name="' + name + '"/>').appendTo(td);
                    if(typeof val != 'undefined')
                        elem.attr('value', val);
                    break;

                case "checkbox":
                    elem = $('<select name="' + name + '"></select>').appendTo(td);
                    var enabled = $('<option name="' + name + '" value="true">Enabled</option>').appendTo(elem);
                    var disabled = $('<option name="' + name + '" value="false">Disabled</option>').appendTo(elem);

                    if(val == true) enabled.attr('selected', 'true');
                    else if(val == false) disabled.attr('selected', 'true');
                    break;

                case "selectImage":
                    elem = $('<input type="hidden" name="' + name + '"/><img name="' + name + '"><a class="select-image-button button-secondary button80" href="#">Select image</a><a class="remove-image-button button-secondary button80" href="#">Remove image</a>').appendTo(td);
                    $(elem[0]).attr("value", val);
                    $(elem[1]).attr("src", val);
                    break;

                case "selectFile":
                    elem = $('<input type="text" name="' + name + '"/><a class="select-image-button button-secondary button80" href="#">Select file</a>').appendTo(td);
                    elem.attr('value', val);
                    break;

                case "dropdown":
                
                    elem = $('<select name="' + name + '"></select>').appendTo(td);

                    for (var i = 0; i < values.length; i++) {
                        var option = $('<option name="' + name + '" value="' + values[i] + '">' + values[i] + '</option>').appendTo(elem);
                        if (val == values[i]) {
                            option.attr('selected', 'true');
                        }
                    }
                    break;

            }

             if(typeof help != 'undefined')
                var p = $('<p class="description">'+help+'</p>').appendTo(td)

        }

        function createShareHtml(prefix, id, name, icon, url, target) {

            if (typeof(target) == 'undefined' || target != "_self")
                target = "_blank";

            var markup = $('<div id="' + id + '"class="share">' + '<h4>Share button ' + id + '</h4>' + '<div class="tabs settings-area">' + '<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">' + '<li><a href="#tabs-1">Icon name</a></li>' + '<li><a href="#tabs-2">Icon css class</a></li>' + '<li><a href="#tabs-3">Link</a></li>' + '<li><a href="#tabs-4">Target</a></li>' + '</ul>' + '<div id="tabs-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom">' + '<div class="field-row">' + '<input id="page-title" name="' + prefix + '[name]" type="text" placeholder="Enter icon name" value="' + name + '" />' + '</div>' + '</div>' + '<div id="tabs-2" class="ui-tabs-panel ui-widget-content ui-corner-bottom">' + '<div class="field-row">' + '<input id="image-path" name="' + prefix + '[icon]" type="text" placeholder="Enter icon CSS class" value="' + icon + '" />' + '</div>' + '</div>' + '<div id="tabs-3" class="ui-tabs-panel ui-widget-content ui-corner-bottom">' + '<div class="field-row">' + '<input id="image-path" name="' + prefix + '[url]" type="text" placeholder="Enter link" value="' + url + '" />' + '</div>' + '</div>' + '<div id="tabs-4" class="ui-tabs-panel ui-widget-content ui-corner-bottom">' + '<div class="field-row">' // + '<input id="image-path" name="'+prefix+'[target]" type="text" placeholder="Enter link" value="'+target+'" />'

                +
                '<select id="social-share" name="' + prefix + '[target]">' // + '<option name="'+prefix+'[target]" value="_self">_self</option>'
                // + '<option name="'+prefix+'[target]" value="_blank">_blank</option>'
                +
                '</select>' + '</div>' + '</div>' + '<div class="submitbox deletediv"><span class="submitdelete deletion">x</span></div>' + '</div>' + '</div>' + '</div>');

            var values = ["_self", "_blank"];
            var select = markup.find('select');

            for (var i = 0; i < values.length; i++) {
                var option = $('<option name="' + prefix + '[target]" value="' + values[i] + '">' + values[i] + '</option>').appendTo(select);
                if (typeof(options["socialShare"][id]) != 'undefined') {
                    if (options["socialShare"][id]["target"] == values[i]) {
                        option.attr('selected', 'true');
                    }
                }
            }

            return markup;
        }

        function getOptionValue(optionName, type) {
            var type = type || 'input'
            var opiton = $(type + "[name='" + optionName + "']")
            return opiton.attr('value');
        }

        function getOption(optionName, type) {
            var type = type || 'input'
            var opiton = $(type + "[name='" + optionName + "']")
            return opiton;
        }

        // console.log(getOptionValue('mode', 'select'))

     

        function setOptionValue(optionName, value, type) {
            var type = type || 'input'
            var $elem = $(type + "[name='" + optionName + "']").attr('value', value).prop('checked', value);

            $("select[name='" + optionName + "']").val(value);
            // $("option[value='"+value+"']").attr("selected", true);

            return $elem
        }

        function setColorOptionValue(optionName, value) {
            var $elem = $("input[name='" + optionName + "']").attr('value', value);
            $elem.wpColorPicker()
            return $elem
        }

       
      

    });
})(jQuery);

function r3d_stripslashes(str) {
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Ates Goral (http://magnetiq.com)
    // +      fixed by: Mick@el
    // +   improved by: marrtins
    // +   bugfixed by: Onno Marsman
    // +   improved by: rezna
    // +   input by: Rick Waldron
    // +   reimplemented by: Brett Zamir (http://brett-zamir.me)
    // +   input by: Brant Messenger (http://www.brantmessenger.com/)
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: stripslashes('Kevin\'s code');
    // *     returns 1: "Kevin's code"
    // *     example 2: stripslashes('Kevin\\\'s code');
    // *     returns 2: "Kevin\'s code"
    return (str + '').replace(/\\(.?)/g, function(s, n1) {
        switch (n1) {
            case '\\':
                return '\\';
            case '0':
                return '\u0000';
            case '':
                return '';
            default:
                return n1;
        }
    });
}