var pluginDir = (function(scripts) {
    var scripts = document.getElementsByTagName('script'),
        script = scripts[scripts.length - 1];
    if (script.getAttribute.length !== undefined) {
        return script.src.split('js/edit_flipbook')[0]
    }
    return script.getAttribute('src', -1).split('js/edit_flipbook')[0]
})();


(function($) {

    $(document).ready(function() {

        PDFJS = pdfjsLib

        $('#real3dflipbook-admin').show()

        var pdfDocument = null
       
        $('.creating-page').hide()

        options = $.parseJSON(window.flipbook)

        function convertStrings(obj) {

            $.each(obj, function(key, value) {
                // console.log(key + ": " + options[key]);
                if (typeof(value) == 'object' || typeof(value) == 'array') {
                    convertStrings(value)
                } else if (!isNaN(value)) {
                    if (obj[key] == "")
                        delete obj[key]
                    else
                        obj[key] = Number(value)
                } else if (value == "true") {
                    obj[key] = true
                } else if (value == "false") {
                    obj[key] = false
                }
            });

        }
        convertStrings(options)

        var title
        if (options.status == "draft") {
            title = 'Add New Flipbook'
        } else {
            title = 'Edit Flipbook'
        }

        $("#edit-flipbook-text").text(title)
        $("#title").val(r3d_stripslashes(options.name))


        addOptionGeneral(
            "mode", 
            "dropdown",         
            "Mode", 
            '<strong>normal</strong> - embedded in a container div<br/><strong>lightbox</strong> - opens in fullscreen overlay on click<br/><strong>fullscreen</strong> - covers entire page',
            ["normal", "lightbox", "fullscreen"]
        );
        
        addOptionGeneral(
            "viewMode", 
            "dropdown",         
            "View mode", 
            '<strong>webgl</strong> - realistic 3D page flip with lights and shadows<br/><strong>3d</strong> - CSS 3D flip<br/><strong>swipe</strong> - horizontal swipe<br/><strong>simple</strong> - no animation',
            ["webgl", "3d", "2d", "swipe", "simple"]
        );
        
        addOptionGeneral( 
            "zoomMin",          
            "text", 
            "Initial zoom", 
            'Initial book zoom, recommended between 0.8 and 1'
        );
        
        addOptionGeneral( 
            "zoomStep",         
            "text", 
            "Zoom step", 
            'Between 1.1 and 4'
        );
        
        addOptionGeneral( 
            "zoomDisabled",     
            "checkbox", 
            "Disable mouse wheel zoom"
        );

        addOptionGeneral( 
            "singlePageMode",   
            "checkbox", 
            "Single page view", 
            'Display one page at a time'
        );

        addOptionGeneral( 
            "pageFlipDuration", 
            "text", 
            "Flip duration", 
            'Duration of flip animation, recommended between 0.5 and 2'
        );

        addOptionGeneral( 
            "sound",            
            "checkbox", 
            "Page flip sound"
        );
        addOptionGeneral( 
            "startPage",        
            "text", 
            "Start page", 
            'Open flipbook at this page at start'
        );

        addOptionGeneral( 
            "deeplinking[enabled]", 
            "checkbox", 
            "Deep linking", 
            'Enables to link to specific page within flipbook, for example if flipbook is at example.com, open page 2 with example.com#2'
        );

        addOptionGeneral( 
            "deeplinking[prefix]", 
            "text", 
            "Deep linking prefix", 
            'Custom deep linking prefix, for example "book1_", link to page 2 will be example.com#book1_2'
        );
        
        addOptionGeneral( 
            "responsiveView",   
            "checkbox",
            "Responsive view", 
            'Switching from two page layout to one page layout if flipbook width is below certain treshold'
        );
        
        addOptionGeneral( 
            "responsiveViewTreshold", 
            "text", 
            "Responsive view treshold", 
            'Treshold (screen width in px) for responsive view feature'
        );

        addOptionGeneral( 
            "pageTextureSize",  
            "text", 
            "PDF page size", 
            'The height of rendered pdf pages in px.'
        );
        
        addOptionGeneral( 
            "textLayer", 
            "checkbox", 
            "PDF text layer", 
            'PDF text selection'
        );
        
        addOptionGeneral( 
            "pdfPageScale", 
            "text", 
            "PDF page scale"
        );
        
        addOptionGeneral( 
            "aspectRatio", 
            "text", 
            "Container responsive ratio", 
            'Container width / height ratio, recommended between 1 and 2'
        );
        
        addOptionGeneral( 
            "thumbnailsOnStart", 
            "checkbox", 
            "Show thumbnails on start"
        );
        
        addOptionGeneral( 
            "contentOnStart", 
            "checkbox", 
            "Show table of content on start"
        );
        
        addOptionGeneral( 
            "tableOfContentCloseOnClick",
             "checkbox", 
             "Close table of content when page is clicked"
        );
        
        addOptionGeneral( 
            "thumbsCloseOnClick", 
            "checkbox", 
            "Close thumbnails when page is clicked"
        );
        
        addOptionGeneral( 
            "autoplayOnStart", 
            "checkbox", 
            "Autoplay on start"
        );
        
        addOptionGeneral( 
            "autoplayInterval", 
            "text", 
            "Autoplay interval (ms)"
        );
        
        addOptionGeneral( 
            "autoplayStartPage", 
            "text", 
            "Autoplay start page"
        );
        
        addOptionGeneral( 
            "rightToLeft", 
            "checkbox", 
            "Right to left mode", 
            'Flipping from right to left (inverted)'
        );
        
        addOptionGeneral( 
            "thumbSize",
             "text", 
             "Thumbnail size", 
             'Thumbnail height for thumbnails view'
        );

        addOptionGeneral( 
            "logoImg",
             "selectImage", 
             "Logo image",
              'Logo image that will be displayed inside the flipbook container'
        );

        addOptionGeneral( 
            "logoUrl",
             "text", 
             "Logo link", 
             'URL that will be opened on logo click'
        );

        addOptionGeneral( 
            "logoCSS", 
            "textarea",
             "Logo CSS",
             'Custom CSS for logo'
        );

        addOptionGeneral( 
            "menuSelector",
            "text",
            "Menu css selector",
            'Example "#menu" or ".navbar". Used with mode "fullscreen" so the flipbook will be resized correctly below the menu'
        );

        addOptionGeneral( 
            "zIndex", 
            "text", 
            "Container z-index",
            'Set z-index of flipbook container'
        );

        addOptionGeneral( 
            'preloaderText', 
            'text', 
            'Preloader text', 
            'Text that will be displayed under the preloader spinner'
        );

        addOptionGeneral( 
            'googleAnalyticsTrackingCode', 
            'text', 
            'Google analytics tracking code'
        );

        addOptionGeneral( 
            "pdfBrowserViewerIfIE",         
            "checkbox",         
            "Download PDF instead of displaying flipbook if browser is Internet Explorer", 
            'For pdf flipbook'
        );

        addOptionMobile( 
            "viewModeMobile", "dropdown",         
            "View mode", 
            'Override default view mode for mobile',
            ["", "webgl", "3d", "2d", "swipe", "simple"]
        );

        addOptionMobile( 
            "pageTextureSizeMobile",  
            "text", 
            "PDF page size", 
            'The height of rendered pdf pages in px.'
        );

        addOptionMobile( 
            "aspectRatioMobile", 
            "text", 
            "Container responsive ratio", 
            'Container width / height ratio, recommended between 1 and 2'
        );

        addOptionMobile( 
            "singlePageModeIfMobile", 
            "checkbox", 
            "Single page view", 
            'Display one page at a time'
        );

        addOptionMobile( 
            "pdfBrowserViewerIfMobile",
            "checkbox", 
            "Use default device pdf viewer instead of flipbook", 
            'Opens pdf file directly in browser, instead of flipbook'
        );

        addOptionMobile(
         "pdfBrowserViewerFullscreen", 
         "checkbox", 
         "Default device pdf viewer fullscreen"
        );

        addOptionMobile( 
            "pdfBrowserViewerFullscreenTarget",
            "dropdown", 
            "Default device pdf viewer target", 
            'Opens pdf file in new tab or in same tab',
            ["_self", "_blank"]
        );

        addOptionMobile( 
            "btnTocIfMobile", 
            "checkbox", 
            "Button table of content"
        );

        addOptionMobile( 
            "btnThumbsIfMobile", 
            "checkbox", 
            "Button thumbnails"
        );

        addOptionMobile( 
            "btnShareIfMobile", 
            "checkbox", 
            "Button share"
        );

        addOptionMobile( 
            "btnDownloadPagesIfMobile", 
            "checkbox", 
            "Button download pages"
        );

        addOptionMobile( 
            "btnDownloadPdfIfMobile", 
            "checkbox", 
            "Button view pdf"
        );

        addOptionMobile( 
            "btnSoundIfMobile", 
            "checkbox", 
            "Button sound"
        );

        addOptionMobile( 
            "btnExpandIfMobile", 
            "checkbox", 
            "Button fullscreen"
        );

        addOptionMobile( 
            "btnPrintIfMobile", 
            "checkbox", 
            "Button print"
        );

        addOptionMobile( 
            "logoHideOnMobile",
             "checkbox", 
             "Hide logo"
        );

        addOptionLightbox( 
            "lightboxCssClass", 
            "text", 
            "Lightbox CSS class", 
            'Add this CSS class to any element that you want to trigger lightbox (Flipbook shortcode needs to be on the same page)'
        );

        addOptionLightbox( 
            "lightboxCSS", 
            "textarea", 
            "Lightbox overlay CSS", 
            'Custom CSS for ligtbox overlay element'
        );

        addOptionLightbox( 
            "lightboxBackground", 
            "color", 
            "Lightbox overlay background", 
            'Lightbox background CSS, for example hex color #999 or rgb color rgb(128,128,128) or rgba color with transparency rgba(0,0,0,0.5) or image url("images/background.jpg")'
        );

        addOptionLightbox( 
            "lightboxContainerCSS", 
            "textarea", 
            "Thumbnail container CSS"
        );
        
        addOptionLightbox( 
            "lightboxThumbnailUrl", 
            "selectImage", 
            "Thumbnail", 
            'Image that will be displayed in place of shortcode, and will trigger lightbox on click'
        );
        
        var $thumbRow = $("input[name='lightboxThumbnailUrl']").parent()
        var $btnGenerateThumb = $('<a class="generate-thumbnail-button button-secondary button80" href="#">Generate thumbnail</a>').appendTo($thumbRow)
        
        addOptionLightbox( 
            "lightboxThumbnailHeight", 
            "text", 
            "Thumbnail height", 
            'Height of thumbnail that will be generated from PDF'
        );

        addOptionLightbox( 
            "lightboxThumbnailUrlCSS", 
            "textarea", 
            "Thumbnail CSS",  
            'Custom CSS for lightbox thumbnail image'
        );

        addOptionLightbox( 
            "lightboxText", 
            "text", 
            "Text link", 
            'Text that will be displayed in place of shortcode'
        );

        addOptionLightbox( 
            "lightboxTextCSS", 
            "textarea", 
            "Text link CSS",
            'Custom CSS for text link'
        );

        addOptionLightbox( 
            "lightboxTextPosition", 
            "dropdown", 
            "Text link position", 
            'Text link above or below the thumbnail',
            ["top", "bottom"]
        );

        addOptionLightbox( 
            "lightBoxOpened", 
            "checkbox", 
            "Opened on start", 
            'Lightbox will open automatically on page load'
        );

        addOptionLightbox( 
            "lightBoxFullscreen", 
            "checkbox", 
            "Openes in fullscreen", 
            'Opening the lightbox will put lightbox element to real fullscreen'
        );

        addOptionLightbox( 
            "lightboxCloseOnClick", 
            "checkbox", 
            "Closes when clicked outside the book", 
            'Close lightbox if clicked on the overlay but outside the book'
        );

        addOptionLightbox( 
            "lightboxMarginV", 
            "text", 
            "Vertical margin",
            'Lightbox overlay vertical margin'
        );

        addOptionLightbox   ( 
            "lightboxMarginH", 
            "text", 
            "Horizontal margin",
            'Lightbox overlay horizontal margin'
        );

        addOption(
            "currentPage", 
            "currentPage[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "currentPage", 
            "currentPage[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnAutoplay", 
            "btnAutoplay[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnAutoplay", 
            "btnAutoplay[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnAutoplay", 
            "btnAutoplay[iconAlt]", 
            "text", 
            "Icon alt CSS class"
        );

        addOption(
            "btnAutoplay", 
            "btnAutoplay[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnNext", 
            "btnNext[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnNext", 
            "btnNext[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnNext", 
            "btnNext[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnFirst", 
            "btnFirst[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnFirst", 
            "btnFirst[icon]", 
            "text", 
            "Button font awesome CSS class"
        );

        addOption(
            "btnFirst", 
            "btnFirst[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnLast", 
            "btnLast[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnLast", 
            "btnLast[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnLast", 
            "btnLast[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnPrev", 
            "btnPrev[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnPrev", 
            "btnPrev[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnPrev", 
            "btnPrev[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnZoomIn", 
            "btnZoomIn[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnZoomIn", 
            "btnZoomIn[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnZoomIn", 
            "btnZoomIn[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnZoomOut", 
            "btnZoomOut[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnZoomOut", 
            "btnZoomOut[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnZoomOut", 
            "btnZoomOut[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnToc", 
            "btnToc[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnToc", 
            "btnToc[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnToc", 
            "btnToc[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnThumbs", 
            "btnThumbs[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnThumbs", 
            "btnThumbs[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnThumbs", 
            "btnThumbs[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnShare", 
            "btnShare[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnShare", 
            "btnShare[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnShare", 
            "btnShare[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnSound", 
            "btnSound[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnSound", 
            "btnSound[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnSound", 
            "btnSound[iconAlt]", 
            "text", 
            "Alt Icon CSS class"
        );

        addOption(
            "btnSound", 
            "btnSound[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnDownloadPages", 
            "btnDownloadPages[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnDownloadPages", 
            "btnDownloadPages[url]", 
            "selectFile", 
            "Url of zip file containing all pages"
        );

        addOption(
            "btnDownloadPages", 
            "btnDownloadPages[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnDownloadPages", 
            "btnDownloadPages[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnDownloadPdf", 
            "btnDownloadPdf[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnDownloadPdf", 
            "btnDownloadPdf[url]", 
            "selectFile", 
            "PDF file url"
        );

        addOption(
            "btnDownloadPdf", 
            "btnDownloadPdf[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnDownloadPdf", 
            "btnDownloadPdf[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnDownloadPdf", 
            "btnDownloadPdf[forceDownload]", 
            "checkbox", 
            "Force download"
        );

        addOption(
            "btnDownloadPdf", 
            "btnDownloadPdf[openInNewWindow]", 
            "checkbox", 
            "Open PDF in new browser window"
        );

        addOption(
            "btnPrint", 
            "btnPrint[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnPrint", 
            "printPdfUrl", 
            "selectFile", 
            "PDF file for printing"
        );

        addOption(
            "btnPrint", 
            "btnPrint[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnPrint", 
            "btnPrint[title]", 
            "text", 
            "Button title"
        );

        addOption(
            "btnExpand", 
            "btnExpand[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnExpand", 
            "btnExpand[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnExpand", 
            "btnExpand[iconAlt]", 
            "text", 
            "Icon CSS class alt"
        );

        addOption(
            "btnExpand", 
            "btnExpand[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnSelect", 
            "btnSelect[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnSelect", 
            "btnSelect[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnSelect", 
            "btnSelect[title]", 
            "text", 
            "Title"
        );

        addOption(
            "btnSearch", 
            "btnSearch[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "btnSearch", 
            "btnSearch[icon]", 
            "text", 
            "Icon CSS class"
        );

        addOption(
            "btnSearch", 
            "btnSearch[title]", 
            "text", 
            "Title"
        );

        addOption(
            "google_plus", 
            "google_plus[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "google_plus", 
            "google_plus[url]", 
            "text", 
            "URL"
        );

        addOption(
            "twitter", 
            "twitter[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "twitter", 
            "twitter[url]", 
            "text", 
            "URL"
        );

        addOption(
            "twitter", 
            "twitter[description]", 
            "text", 
            "Description"
        );

        addOption(
            "facebook", 
            "facebook[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "facebook", 
            "facebook[url]", 
            "text", 
            "URL"
        );

        addOption(
            "facebook", 
            "facebook[description]", 
            "text", 
            "Description"
        );

        addOption(
            "facebook", 
            "facebook[title]", 
            "text", 
            "Title"
        );

        addOption(
            "facebook", 
            "facebook[image]", 
            "text", 
            "Image"
        );

        addOption(
            "facebook", 
            "facebook[caption]", 
            "text", 
            "Caption"
        );

        addOption(
            "pinterest", 
            "pinterest[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "pinterest", 
            "pinterest[url]", 
            "text", 
            "URL"
        );

        addOption(
            "pinterest", 
            "pinterest[image]", 
            "text", 
            "Image"
        );

        addOption(
            "pinterest", 
            "pinterest[description]", 
            "text", 
            "Description"
        );

        addOption(
            "email", 
            "email[enabled]", 
            "checkbox", 
            "Enabled"
        );

        addOption(
            "email", 
            "email[url]", 
            "text", 
            "URL"
        );

        addOption(
            "email", 
            "email[description]", 
            "text", 
            "Description"
        );

        addOptionWebgl(
            "lights", 
            "checkbox", 
            "Lights",
            'Realistic lightning, disable for faster performance'
        );

        // addOptionWebgl(
        //     "lightColor", 
        //     "text", 
        //     "Light color", 
        //     "0xFFFFFF"
        // );

        addOptionWebgl(
            "lightPositionX", 
            "text", 
            "Light pposition x", 
            'Light X position, between -500 and 500'
        );

        addOptionWebgl(
            "lightPositionY", 
            "text", 
            "Light position y", 
            'Light Y position, between -500 and 500'
        );

        addOptionWebgl(
            "lightPositionZ", 
            "text", 
            "Light position z", 
            'Light Z position, between 1000 and 2000'
        );

        addOptionWebgl(
            "lightIntensity", 
            "text", 
            "Light intensity", 
            'Light intensity, between 0 and 1'
        );

        addOptionWebgl(
            "shadows", 
            "checkbox", 
            "Shadows", 
            'Realistic page shadows, disable for faster performance'
        );

        addOptionWebgl(
            "shadowMapSize", 
            "text", 
            "Shadow Map Size", 
            'Shadow quality, 1024 or 2048 or 4096'
        );

        addOptionWebgl(
            "shadowOpacity", 
            "text", 
            "Shadow opacity", 
            'Shadow darkness, between 0 and 1'
        );

        addOptionWebgl(
            "shadowDistance", 
            "text", 
            "Shadow plane distance", 
            'Distance of shadow from the page, between 10 and 20'
        );

        addOptionWebgl(
            "pageHardness", 
            "text", 
            "Page hardness", 
            'Between 1 and 5'
        );

        addOptionWebgl(
            "coverHardness", 
            "text", 
            "Cover hardness", 
            'Between 1 and 5'
        );

        addOptionWebgl(
            "pageRoughness", 
            "text", 
            "Page material roughness", 
            'Between 0 and 1'
        );

        addOptionWebgl(
            "pageMetalness", 
            "text", 
            "Page material metalness", 
            'Between 0 and 1'
        );

        addOptionWebgl(
            "pageSegmentsW", 
            "text", 
            "Page segments W", 

            'Number of page segments horizontally'
        );

        addOptionWebgl(
            "pageSegmentsH", 
            "text", 
            "Page segments H", 
            'Number of page polygons vertically'
        );

        addOptionWebgl(
            "pageMiddleShadowSize", 
            "text", 
            "Page middle shadow size", 
            'Shadow in the middle of the book'
        );

        addOptionWebgl(
            "pageMiddleShadowColorL", 
            "color", 
            "Left middle shadow color"
        );

        addOptionWebgl(
            "pageMiddleShadowColorR", 
            "color", 
            "Right middle shadow color"
        );

        addOptionWebgl(
            "antialias", 
            "checkbox", 
            "Antialiasing", 
            'Disable for faster performance'
        );

        addOptionWebgl(
            "pan", 
            "text", 
            "Camera pan angle", 
            'Between -10 and 10'
        );

        addOptionWebgl(
            "tilt", 
            "text", 
            "Camera tilt angle", 
            'Between -30 and 0'
        );

        addOptionWebgl(
            "rotateCameraOnMouseDrag", 
            "checkbox", 
            "Rotate camera on mouse drag"
        );

        addOptionWebgl(
            "panMax", 
            "text", 
            "Camera pan max", 
            'Max pan angle for mouse drag rotate, between 0 and 20'
        );

        addOptionWebgl(
            "panMin", 
            "text", 
            "Camera pan min", 
            'Min pan angle for mouse drag rotate, between -20 and 0'
        );

        addOptionWebgl(
            "tiltMax", 
            "text", 
            "Camera tilt max", 
            'Max tilt angle for mouse drag rotate, between -60 and 0'
        );

        addOptionWebgl(
            "tiltMin", 
            "text", 
            "Camera tilt min", 
            'Min tilt angle for mouse drag rotate, between -60 and 0'
        );

        //UI
        addOption(
            "menu-bar", 
            "menuBackground", 
            "color", 
            "Background color"
        );

        addOption(
            "menu-bar", 
            "menuShadow", 
            "text", 
            "Shadow", 
            'Custom CSS'
        );

        addOption(
            "menu-bar", 
            "menuMargin", 
            "text", 
            "Margin"
        );

        addOption(
            "menu-bar", 
            "menuPadding", 
            "text", 
            "Padding"
        );

        addOption(
            "menu-bar", 
            "menuOverBook", 
            "checkbox", 
            "Menu over book", 
            'Menu on top of book (overlay)'
        );

        addOption(
            "menu-bar", 
            "hideMenu", 
            "checkbox", 
            "Hide menu", 
            'Hide menu completely'
        );

        addOption(
            "menu-buttons", 
            "menuAlignHorizontal", 
            "dropdown", 
            "Horizontal position", 
            "",
            ['right', 'left', 'center']
        );

        addOption(
            "menu-buttons", 
            "btnColor", 
            "color", 
            "Color"
        );

        addOption(
            "menu-buttons", 
            "btnBackground", 
            "color", 
            "Background color"
        );

        addOption(
            "menu-buttons", 
            "btnRadius", 
            "text", 
            "Radius", 
            'px'
        );

        addOption(
            "menu-buttons", 
            "btnMargin", 
            "text", 
            "Margin", 
            'px'
        );

        addOption(
            "menu-buttons", 
            "btnSize", 
            "text", 
            "Size", 
            'Size of menu buttons, between 8 and 20'
        );

        addOption(
            "menu-buttons", 
            "btnPaddingV", 
            "text", 
            "Padding vertical", 
            'Padding vertical of menu buttons, between 0 and 20'
        );

        addOption(
            "menu-buttons", 
            "btnPaddingH", 
            "text", 
            "Padding horizontal", 
            'Padding horizontal of menu buttons, between 0 and 20'
        );

        addOption(
            "menu-buttons", 
            "btnShadow", 
            "text", 
            "Box shadow", 
            'CSS value'
        );

        addOption(
            "menu-buttons", 
            "btnTextShadow", 
            "text", 
            "Text shadow", 
            'CSS value'
        );

        addOption(
            "menu-buttons", 
            "btnBorder", 
            "text", 
            "Border", 
            'CSS value'
        );

        addOption(
            "side-buttons", 
            "sideBtnColor", 
            "color", 
            "Color"
        );

        addOption(
            "side-buttons", 
            "sideBtnBackground", 
            "color", 
            "Background color"
        );

        addOption(
            "side-buttons", 
            "sideBtnRadius", 
            "text", 
            "Radius", 
            'px'
        );

        addOption(
            "side-buttons", 
            "sideBtnMargin", 
            "text", 
            "Margin", 
            'px'
        );

        addOption(
            "side-buttons", 
            "sideBtnSize", 
            "text", 
            "Size", 
            'Side buttons margin size, between 8 and 50'
        );

        addOption(
            "side-buttons", 
            "sideBtnPaddingV", 
            "text", 
            "Padding vertical", 
            'Side buttons padding vertical, between 0 and 10'
        );

        addOption(
            "side-buttons", 
            "sideBtnPaddingH", 
            "text", 
            "Padding horizontal", 
            'Side buttons padding horizontal, between 0 and 10'
        );

        addOption(
            "side-buttons", 
            "sideBtnShadow", 
            "text", 
            "Box shadow", 
            'CSS value'
        );

        addOption(
            "side-buttons", 
            "sideBtnTextShadow", 
            "text", 
            "Text shadow", 
            'CSS value'
        );

        addOption(
            "side-buttons", 
            "sideBtnBorder", 
            "text", 
            "Border", 
            'CSS value'
        );

        addOption(
            "current-page", 
            "currentPagePositionV", 
            "dropdown", 
            'Current page display vertical position',
            "Vertical position", ["top", "bottom"]
        );

        addOption(
            "current-page", 
            "currentPagePositionH", 
            "dropdown", 
            "Horizontal position",
            'Current page display horizontal position',
            ["left", "right"]
        );

        addOption(
            "current-page", 
            "currentPageMarginV", 
            "text", 
            "Vertical margin", 
            'Between 0 and 10'
        );

        addOption(
            "current-page", 
            "currentPageMarginH", 
            "text", 
            "Horizontal margin", 
            'Between 0 and 10'
        );

        addOption(
            "close-button", 
            "closeBtnColor", 
            "color", 
            "Color"
        );

        addOption(
            "close-button", 
            "closeBtnBackground", 
            "color", 
            "Background color"
        );

        addOption(
            "close-button", 
            "closeBtnRadius", 
            "text", 
            "Radius", 
            'CSS value'
        );

        addOption(
            "close-button", 
            "closeBtnMargin", 
            "text", 
            "Margin", 

            'CSS value'
        );

        addOption(
            "close-button", 
            "closeBtnSize", 
            "text", 
            "Size", 
            'Between 8 and 30'
        );

        addOption(
            "close-button", 
            "closeBtnPadding", 
            "text", 
            "Padding", 
            'Between 0 and 10'
        );

        addOption(
            "close-button", 
            "closeBtnTextShadow", 
            "text", 
            "Text shadow", 
            'CSS value'
        );

        addOption(
            "close-button", 
            "closeBtnBorder", 
            "text", 
            "Border", 
            'CSS value'
        );

        addOption(
            "ui", 
            "skin", 
            "dropdown", 
            "Skin", 
            '',
            ["light", "dark", 'lightgrey', 'darkgrey']
        );

        addOption(
            "ui", 
            "sideNavigationButtons", 
            "checkbox", 
            "Side navigation buttons", 
            'Show navigation buttons on the sides'
        );

        addOption(
            "bg", 
            "backgroundColor", 
            "color", 
            "Background color"
        );

        addOption(
            "bg", 
            "backgroundPattern", 
            "selectImage", 
            "Background image (repeated)", 
            'Flipbook container background image that is repeated'
        );

        addOption(
            "bg", 
            "backgroundTransparent", 
            "checkbox", 
            "Transparent", 
            'Flipbook container will have transparent background'
        );

        //translate

        addOption(
            "translate", 
            "strings[print]", 
            "text", 
            "Print"
        );

        addOption(
            "translate", 
            "strings[printLeftPage]", 
            "text", 
            "Print left page"
        );

        addOption(
            "translate", 
            "strings[printRightPage]", 
            "text", 
            "Print right page"
        );

        addOption(
            "translate", 
            "strings[printCurrentPage]", 
            "text", 
            "Print current page"
        );

        addOption(
            "translate", 
            "strings[printAllPages]", 
            "text", 
            "Print all pages"
        );

        addOption(
            "translate", 
            "strings[download]", 
            "text", 
            "Download"
        );

        addOption(
            "translate", 
            "strings[downloadLeftPage]", 
            "text", 
            "Download left page"
        );

        addOption(
            "translate", 
            "strings[downloadRightPage]", 
            "text", 
            "Download right page"
        );

        addOption(
            "translate", 
            "strings[downloadCurrentPage]", 
            "text", 
            "Download current page"
        );

        addOption(
            "translate", 
            "strings[downloadAllPages]", 
            "text", 
            "Download all pages"
        );

        addOption(
            "translate", 
            "strings[bookmarks]", 
            "text", 
            "Bookmarks"
        );

        addOption(
            "translate", 
            "strings[bookmarkLeftPage]", 
            "text", 
            "Bookmark left page"
        );

        addOption(
            "translate", 
            "strings[bookmarkRightPage]", 
            "text", 
            "Bookmark right page"
        );

        addOption(
            "translate", 
            "strings[bookmarkCurrentPage]", 
            "text", 
            "Bookmark current page"
        );

        addOption(
            "translate", 
            "strings[search]", 
            "text", 
            "Search"
        );

        addOption(
            "translate", 
            "strings[findInDocument]", 
            "text", 
            "Find in document"
        );

        addOption(
            "translate", 
            "strings[pagesFoundContaining]", 
            "text", 
            "pages found containing"
        );

        addOption(
            "translate", 
            "strings[thumbnails]", 
            "text", 
            "Thumbnails"
        );

        addOption(
            "translate", 
            "strings[tableOfContent]", 
            "text", 
            "Table of content"
        );

        addOption(
            "translate", 
            "strings[share]", 
            "text", 
            "Share"
        );

        addOption(
            "translate", 
            "strings[pressEscToClose]", 
            "text", 
            "Press ESC to close"
        );

        $('input.alpha-color-picker').alphaColorPicker()


        var ui_themes = { "default": { "skin": "light", "sideNavigationButtons": true, "menuMargin": 0, "menuPadding": 0, "menuAlignHorizontal": "center", "menuShadow": "0 0 6px rgba(0,0,0,0.16), 0 0 6px rgba(0,0,0,0.23)", "menuBackground": "", "menuOverBook": false, "btnSize": 12, "btnRadius": 4, "btnMargin": 4, "btnPaddingV": 10, "btnPaddingH": 10, "btnShadow": "", "btnTextShadow": "", "btnBorder": "", "btnColor": "", "btnBackground": "none", "sideBtnSize": 30, "sideBtnRadius": 0, "sideBtnMargin": 0, "sideBtnPaddingV": 5, "sideBtnPaddingH": 5, "sideBtnShadow": "", "sideBtnTextShadow": "", "sideBtnBorder": "", "sideBtnColor": "#FFF", "sideBtnBackground": "rgba(0,0,0,.3)", "currentPagePositionV": "top", "currentPagePositionH": "left", "currentPageMarginV": 5, "currentPageMarginH": 5, "closeBtnSize": 20, "closeBtnRadius": 0, "closeBtnMargin": 0, "closeBtnPadding": 10, "closeBtnTextShadow": "", "closeBtnColor": "#fff", "closeBtnBackground": "rgba(0,0,0,.3)", "closeBtnBorder": "" }, "demo1": { "skin": "light", "sideNavigationButtons": "true", "menuMargin": "0", "menuPadding": "0", "menuAlignHorizontal": "center", "menuShadow": "0 0 6px rgba(0,0,0,0.16), 0 0 6px rgba(0,0,0,0.23)", "menuBackground": "", "menuOverBook": "false", "btnSize": "16", "btnRadius": "0", "btnMargin": "0", "btnPaddingV": "10", "btnPaddingH": "10", "btnShadow": "", "btnTextShadow": "", "btnBorder": "", "btnColor": "", "btnBackground": "none", "sideBtnSize": "40", "sideBtnRadius": "0", "sideBtnMargin": "5", "sideBtnPaddingV": "10", "sideBtnPaddingH": "10", "sideBtnShadow": "", "sideBtnTextShadow": "", "sideBtnBorder": "none", "sideBtnColor": "#dd4040", "sideBtnBackground": "none", "currentPagePositionV": "top", "currentPagePositionH": "left", "currentPageMarginV": "5", "currentPageMarginH": "5" }, "demo2": { "skin": "light", "sideNavigationButtons": "true", "menuMargin": "0", "menuPadding": "0", "menuAlignHorizontal": "center", "menuShadow": "none", "menuBackground": "none", "menuOverBook": "true", "btnSize": "14", "btnRadius": "4", "btnMargin": "4", "btnPaddingV": "10", "btnPaddingH": "10", "btnShadow": "", "btnTextShadow": "", "btnBorder": "", "btnColor": "#ffffff", "btnBackground": "rgba(0,0,0,.2)", "sideBtnSize": "30", "sideBtnRadius": "0", "sideBtnMargin": "0", "sideBtnPaddingV": "5", "sideBtnPaddingH": "5", "sideBtnShadow": "", "sideBtnTextShadow": "", "sideBtnBorder": "", "sideBtnColor": "#898585", "sideBtnBackground": "none", "currentPagePositionV": "top", "currentPagePositionH": "left", "currentPageMarginV": "5", "currentPageMarginH": "5" }, "demo3": { "skin": "light", "sideNavigationButtons": "false", "menuMargin": "0", "menuPadding": "5px", "menuAlignHorizontal": "right", "menuShadow": "none", "menuBackground": "none", "menuOverBook": "true", "btnSize": "12", "btnRadius": "0", "btnMargin": "0", "btnPaddingV": "12", "btnPaddingH": "12", "btnShadow": "", "btnTextShadow": "0 0 2px rgba(0,0,0,.5)", "btnBorder": "", "btnColor": "#ffffff", "btnBackground": "rgba(0,0,0,.3)", "sideBtnSize": "60", "sideBtnRadius": "50", "sideBtnMargin": "0", "sideBtnPaddingV": "5", "sideBtnPaddingH": "5", "sideBtnShadow": "", "sideBtnTextShadow": "", "sideBtnBorder": "", "sideBtnColor": "#adadad", "sideBtnBackground": "none", "currentPagePositionV": "top", "currentPagePositionH": "left", "currentPageMarginV": "5", "currentPageMarginH": "5" }, "demo4": { "skin": "light", "sideNavigationButtons": "true", "menuMargin": "0", "menuPadding": "0", "menuAlignHorizontal": "center", "menuShadow": "0 0 6px rgba(0,0,0,0.16), 0 0 6px rgba(0,0,0,0.23)", "menuBackground": "", "menuOverBook": "false", "btnSize": "12", "btnRadius": "0", "btnMargin": "0", "btnPaddingV": "10", "btnPaddingH": "10", "btnShadow": "", "btnTextShadow": "", "btnBorder": "", "btnColor": "", "btnBackground": "none", "sideBtnSize": "40", "sideBtnRadius": "0", "sideBtnMargin": "0", "sideBtnPaddingV": "", "sideBtnPaddingH": "", "sideBtnShadow": "", "sideBtnTextShadow": "", "sideBtnBorder": "", "sideBtnColor": "#000", "sideBtnBackground": "rgba(255,255,255,.2)", "currentPagePositionV": "top", "currentPagePositionH": "left", "currentPageMarginV": "5", "currentPageMarginH": "5" }, "demo5": { "skin": "dark", "sideNavigationButtons": "true", "menuMargin": "0", "menuPadding": "10", "menuAlignHorizontal": "center", "menuShadow": "none", "menuBackground": "linear-gradient(to top,rgba(0,0,0,0.65) 0%,transparent 100%)", "menuOverBook": "true", "btnSize": "14", "btnRadius": "40", "btnMargin": "5", "btnPaddingV": "10", "btnPaddingH": "10", "btnShadow": "", "btnTextShadow": "", "btnBorder": "2px solid #f4be0e", "btnColor": "#f4be0e", "btnBackground": "none", "sideBtnSize": "30", "sideBtnRadius": "80", "sideBtnMargin": "5", "sideBtnPaddingV": "10", "sideBtnPaddingH": "10", "sideBtnShadow": "0 0 2px #000", "sideBtnTextShadow": "0 0 2px #000", "sideBtnBorder": "2px solid #f4be0e", "sideBtnColor": "#f4be0e", "sideBtnBackground": "none", "currentPagePositionV": "top", "currentPagePositionH": "left", "currentPageMarginV": "5", "currentPageMarginH": "5" }, "demo6": { "skin": "dark", "sideNavigationButtons": "true", "menuMargin": "0", "menuPadding": "10", "menuAlignHorizontal": "center", "menuShadow": "none", "menuBackground": "linear-gradient(to top,rgba(0,0,0,0.65) 0%,transparent 100%)", "menuOverBook": "false", "btnSize": "16", "btnRadius": "40", "btnMargin": "5", "btnPaddingV": "12", "btnPaddingH": "12", "btnShadow": "", "btnTextShadow": "", "btnBorder": "2px solid #f4be0e", "btnColor": "#f4be0e", "btnBackground": "none", "sideBtnSize": "50", "sideBtnRadius": "80", "sideBtnMargin": "5", "sideBtnPaddingV": "", "sideBtnPaddingH": "", "sideBtnShadow": "0 0 2px #000", "sideBtnTextShadow": "0 0 2px #000", "sideBtnBorder": "2px solid #f4be0e", "sideBtnColor": "#f4be0e", "sideBtnBackground": "none" }, "demo7": { "skin": "light", "sideNavigationButtons": "true", "menuMargin": "0", "menuPadding": "15", "menuAlignHorizontal": "center", "menuShadow": "none", "menuBackground": "none", "menuOverBook": "true", "btnSize": "16", "btnRadius": "4", "btnMargin": "2", "btnPaddingV": "10", "btnPaddingH": "10", "btnShadow": "", "btnTextShadow": "", "btnBorder": "", "btnColor": "#212121", "btnBackground": "rgba(255,255,255,0.7)", "sideBtnSize": "40", "sideBtnRadius": "0", "sideBtnMargin": "0", "sideBtnPaddingV": "5", "sideBtnPaddingH": "5", "sideBtnShadow": "", "sideBtnTextShadow": "", "sideBtnBorder": "", "sideBtnColor": "#898585", "sideBtnBackground": "none", "currentPagePositionV": "top", "currentPagePositionH": "left", "currentPageMarginV": "5", "currentPageMarginH": "5" }, "demo8": { "skin": "light", "sideNavigationButtons": "true", "menuMargin": "5", "menuPadding": "0", "menuAlignHorizontal": "right", "menuShadow": "none", "menuBackground": "none", "menuOverBook": "true", "btnSize": "12", "btnRadius": "4", "btnMargin": "2", "btnPaddingV": "10", "btnPaddingH": "10", "btnShadow": "", "btnTextShadow": "", "btnBorder": "", "btnColor": "#ffffff", "btnBackground": "rgba(0,0,0,.3)", "sideBtnSize": "30", "sideBtnRadius": "4", "sideBtnMargin": "0", "sideBtnPaddingV": "", "sideBtnPaddingH": "", "sideBtnShadow": "", "sideBtnTextShadow": "", "sideBtnBorder": "", "sideBtnColor": "#ffffff", "sideBtnBackground": "rgba(0,0,0,.3)", "currentPagePositionV": "bottom", "currentPagePositionH": "left", "currentPageMarginV": "5", "currentPageMarginH": "5" } }

        $("#flipbook-theme").change(function(e) {

            var name = $("#flipbook-theme").find(":selected").text();
            var obj = ui_themes[name]
            for (var key in obj) {
                setOptionValue(key, obj[key])
            }
        })

        if (options.pdfUrl ) {
           
            setOptionValue('pdfUrl', options.pdfUrl)

            previewPDFPages(options.pdfUrl)
        }

        function previewPDFPages(pdfUrl){

            // console.log("previewPDFPages : "+pdfUrl)

            PDFJS.GlobalWorkerOptions.workerSrc = pluginDir + 'js/pdf.worker.min.js'

            PDFJS.getDocument(pdfUrl, null, false).then(function(pdf) {

                pdfDocument = pdf
                creatingPage = 1
                loadPageFromPdf(pdf)

                pdf.getPage(1).then(function(page){
                    generateLightboxThumbnail(page)
                })

                return;

                pdf.getPage(1).then(function(page){
                        var v1 = page.getViewport(1)
                        var w1 = v1.width
                        var h1 = v1.height
                        if(pdf.numPages > 1){
                            pdf.getPage(2).then(function(){
                                var v2 = page.getViewport(1)
                                var w2 = v2.width
                                var h2 = v2.height
                            })
                        }
                })

            });

        }

        function updateSaveBar() {

            if ((window.innerHeight + window.scrollY) >= (document.body.scrollHeight - 50)) {

                $("#r3d-save").removeClass("r3d-save-sticky")
                $("#r3d-save-holder").hide()

            } else {

                $("#r3d-save").addClass("r3d-save-sticky")
                $("#r3d-save-holder").show()

            }

        }

        $('#real3dflipbook-admin .nav-tab').click(function(e) {
            e.preventDefault()
            $('#real3dflipbook-admin .tab-active').hide()
            $('.nav-tab-active').removeClass('nav-tab-active')
            var a = jQuery(this).addClass('nav-tab-active')
            var id = "#" + a.attr('data-tab')
            jQuery(id).addClass('tab-active').fadeIn()
            window.location.hash = a.attr('data-tab').split("-")[1]
            updateSaveBar()

        })

        $('#real3dflipbook-admin .nav-tab').focus(function(e) {

            this.blur()
        })

        if(window.location.hash){
            $($('.nav-tab[data-tab="tab-'+window.location.hash.split("#")[1]+'"]')[0]).trigger('click')
        }else{
            $($('#real3dflipbook-admin .nav-tab')[0]).trigger('click')
        }


        function sortOptions(){

            function sortTocItems(tocItems, prefix){
                var prefix = prefix || 'tableOfContent'
                for (var i = 0; i < tocItems.length; i++) {
                    $item = $(tocItems[i])
                    $item.find('.toc-title').attr('name', prefix + '['+i+'][title]')
                    $item.find('.toc-page').attr('name', prefix + '['+i+'][page]')

                    var $items = $item.children('.toc-item-wrapper')
                    if($items.length > 0){
                        sortTocItems($items, prefix + '['+i+'][items]')

                    }
                    
                }

            }
  
            var tocItems = $("#toc-items").children(".toc-item-wrapper")
            sortTocItems(tocItems)
            

            var pages = $('#pages-container .page')

            if(pages.length > 0){
                for (var i = 0; i < pages.length; i++) {
                    $item = $(pages[i])
                    $item.find('#page-src').attr('name', 'pages['+i+'][src]')
                    $item.find('#page-thumb').attr('name', 'pages['+i+'][thumb]')
                    $item.find('#page-title').attr('name', 'pages['+i+'][title]')
                    $item.find('#page-html').attr('name', 'pages['+i+'][htmlContent]')
                }
            }


        }

        var $form = $('#real3dflipbook-options-form')

        if(options.status == "draft")
            $('.create-button').show()
        else
            $('.save-button').show()

        $('.flipbook-reset-defaults').click(function(e) {
            e.preventDefault()
            var inputs = $form.find('.global-option')
            inputs.each(function(){
                console.log(this)
                $(this).val('')
            })

            
        })

        $('.flipbook-preview').click(function(e) {

            e.preventDefault()

            sortOptions()

            $form.find('.spinner').css('visibility', 'visible')

            $form.find('.save-button').prop('disabled', 'disabled').css('pointer-events', 'none')

            // var data = $form.serialize() + '&action=r3d_preview'

            var data = 'action=r3d_preview'
            var arr = $form.serializeArray()

            arr.forEach( function(element, index) {

                if(element.value != '') data += ('&' + element.name + '=' + encodeURIComponent(element.value.trim()))

            });


            // var previewOptions = $form.serializeArray()
            // var lightboxElement = $('<p></p>')
            //         previewOptions.lightBox = true
            //        previewOptions.lightBoxOpened = true
            //        debugger
            //         lightboxElement.flipBook(previewOptions)

            //         return

            $.ajax({
                type: "POST",
                url: $form.attr('action'), //.replace('admin-ajax','admin'),
                data: data,
                success: function(response, textStatus, jqXHR) {

                    console.log(response);
                    // var books = $.parseJSON(flipbooks);

                    $form.find('.spinner').css('visibility', 'hidden')
                    $form.find('.save-button').prop('disabled', '').css('pointer-events', 'auto')

                    var o = $.parseJSON(response)
                    convertStrings(o)

                    o.assets = {
                        preloader: pluginDir + "images/preloader.jpg",
                        left: pluginDir + "images/left.png",
                        overlay: pluginDir + "images/overlay.jpg",
                        flipMp3: pluginDir + "mp3/turnPage.mp3",
                        shadowPng: pluginDir + "images/shadow.png"
                    };

                    o.pages = o.pages || []

                    for (var i = 0; i < o.pages.length; i++) {
                        o.pages[i].htmlContent = unescape(o.pages[i].htmlContent)
                    }

                    if (o.pages.length < 1 && !o.pdfUrl) {
                        alert('Flipbook has no pages!')
                        e.preventDefault()
                        return false
                    }

                    var lightboxElement = $('<p></p>')
                    o.lightBox = true
                    o.lightBoxOpened = true
                    lightboxElement.flipBook(o)

                    $(window).trigger('resize')

                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                }
            })

        });

        $form.submit(function(e) {

            e.preventDefault();

            sortOptions()

            $form.find('.spinner').css('visibility', 'visible')

            $form.find('.save-button').prop('disabled', 'disabled').css('pointer-events', 'none')
            $form.find('.create-button').prop('disabled', 'disabled').css('pointer-events', 'none')
            
            var data = 'action=r3d_save'
            var arr = $form.serializeArray()

            arr.forEach( function(element, index) {

                if(element.value != '') data += ('&' + element.name + '=' + encodeURIComponent(element.value.trim()))

            });

            data += ('&bookId=' + options.id + '&security=' + options.security + '&id=' + options.id + '&date=' + encodeURIComponent(options.date) )

            if(options.status == "draft")
                data  += '&status=published';
                
            $.ajax({

                type: "POST",
                url: $form.attr('action'), //.replace('admin-ajax','admin'),
                data: data,

                success: function(data, textStatus, jqXHR) {

                    $('.spinner').css('visibility', 'hidden')
                    $('.save-button').prop('disabled', '').css('pointer-events', 'auto')
                    $('.create-button').hide()
                    $('.save-button').show()
                    $("#edit-flipbook-text").text("Edit Flipbook")

                    removeAllNotices()
                    if(options.status == "draft"){
                        addNotice("Flipbook published")
                        options.status = "published"
                   } else{
                        addNotice("Flipbook updated")
                   }

                },

                error: function(XMLHttpRequest, textStatus, errorThrown) {

                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);

                }
            })

        })

        $(window).scroll(function() {
            updateSaveBar()
        })

        $(window).resize(function() {
            updateSaveBar()
        })

        updateSaveBar()

        function unsaved() { // $('.unsaved').show()
        }

        function addOptionGeneral(name, type, desc, help, values){
            addOption('general',name, type, desc, help, values)
        }

        function addOptionMobile(name, type, desc, help, values){
            addOption('mobile',name, type, desc, help, values)
        }

        function addOptionLightbox(name, type, desc, help, values){
            addOption('lightbox',name, type, desc, help, values)
        }

        function addOptionWebgl(name, type, desc, help, values){
            addOption('webgl',name, type, desc, help, values)
        }

        function addOption(section, name, type, desc, help, values){

            // console.log(options.globals[name])

            var defaultValue = options.globals[name];

            if(typeof defaultValue == 'undefined')
                defaultValue = ""

            if(name.indexOf("[") != -1)
                defaultValue = options.globals[name.split("[")[0]][name.split("[")[1].split("]")[0]] || ""

            var val = options[name]
            if (options[name.split("[")[0]] && name.indexOf("[") != -1 && typeof(options[name.split("[")[0]]) != 'undefined') {
                 val = options[name.split("[")[0]][name.split("[")[1].split("]")[0]]
            } 

            //val = val || defaultValue
            if(typeof val == 'string')
                val = r3d_stripslashes(val)

            var table = $("#flipbook-" + section + "-options");
            var tableBody = table.find('tbody');
            var row = $('<tr valign="top"  class="field-row"></tr>').appendTo(tableBody);
            var th = $('<th scope="row">' + desc + '</th>').appendTo(row);
            var td = $('<td></td>').appendTo(row);
            var elem

            switch (type) {

                case "text":
                    elem = $('<input type="text" name="' + name + '" placeholder="Global setting"/>').appendTo(td);
                    if(typeof val != 'undefined')
                        elem.attr('value', val);
                    elem.addClass("global-option")
                    break;

                case "color":
                    elem = $('<input type="text" name="' + name + '" class="alpha-color-picker" placeholder="Global setting"/>').appendTo(td);
                    elem.attr('value', val);
                    elem.addClass("global-option")
                    break;

                case "textarea":
                    elem = $('<textarea name="' + name + '" placeholder="Global setting"/>').appendTo(td);
                    if(typeof val != 'undefined')
                        elem.attr('value', val);
                    elem.addClass("global-option")
                    break;

                case "checkbox":
                    elem = $('<select name="' + name + '"></select>').appendTo(td);
                    var globalSetting = $('<option name="' + name + '" value="">Global setting</option>').appendTo(elem);
                    var enabled = $('<option name="' + name + '" value="true">Enabled</option>').appendTo(elem);
                    var disabled = $('<option name="' + name + '" value="false">Disabled</option>').appendTo(elem);

                    if(val == true) enabled.attr('selected', 'true');
                    else if(val == false) disabled.attr('selected', 'true');
                    else globalSetting.attr('selected', 'true');
                    elem.addClass("global-option")
                    break;

                case "selectImage":
                    elem = $('<input type="hidden" name="' + name + '"/><img name="' + name + '"><a class="select-image-button button-secondary button80" href="#">Select image</a><a class="remove-image-button button-secondary button80" href="#">Remove image</a>').appendTo(td);
                    $(elem[0]).attr("value", val);
                    $(elem[1]).attr("src", val);
                    break;

                case "selectFile":
                    elem = $('<input type="text" name="' + name + '"/><a class="select-image-button button-secondary button80" href="#">Select file</a>').appendTo(td);
                    elem.attr('value', val);
                    break;

                case "dropdown":
                
                    elem = $('<select name="' + name + '"></select>').appendTo(td);

                    var globalSetting = $('<option name="' + name + '" value="">Global setting</option>')
                    .appendTo(elem)
                    .attr('selected', 'true');

                    for (var i = 0; i < values.length; i++) {
                        var option = $('<option name="' + name + '" value="' + values[i] + '">' + values[i] + '</option>').appendTo(elem);
                        if (val == values[i]) {
                            option.attr('selected', 'true');
                        }
                    }
                    elem.addClass("global-option")
                    break;

            }

            if(type == 'checkbox')
                defaultValue = defaultValue ? 'Enabled' : 'Disabled'

            if(type != 'selectImage' && type != 'selectFile')
                $('<span class="default-setting">Global setting : <strong>'+defaultValue+'</strong></span>').appendTo(td)

             if(typeof help != 'undefined')
                var p = $('<p class="description">'+help+'</p>').appendTo(td)

        }

        //for all pages in  options.pages create page 
        if (!getOptionValue('pdfUrl')) {

            if(options.pages){

                for (var i = 0; i < options.pages.length; i++) {
                    var page = options.pages[i];
                    var pagesContainer = $("#pages-container");
                    var pageItem = createPageHtml(i + 1, page.title, page.src, page.thumb, page.htmlContent);
                    pageItem.appendTo(pagesContainer);

                }
            }

            $('.page-delete').show()
            $('.replace-page').show()

            $('.page').click(function(e) {
                expandPage($(this).attr("id"))
            })

            generateLightboxThumbnail()
        }

        if (options.socialShare == null)
            options.socialShare = [];

        for (var i = 0; i < options.socialShare.length; i++) {
            var share = options.socialShare[i];
            var shareContainer = $("#share-container");
            var shareItem = createShareHtml(i, share.name, share.icon, share.url, share.target);
            shareItem.appendTo(shareContainer);

        }

        if (options.tableOfContent == null)
            options.tableOfContent = [];

        for (var i = 0; i < options.tableOfContent.length; i++) {

            var item = options.tableOfContent[i];
            var tocContainer = $("#toc-items");
            var tocItem = createTocItem(item.title, item.page, item.items);
            tocItem.appendTo(tocContainer);


        }

        // $(".tabs").tabs();
        $(".ui-sortable").sortable();
        addListeners();

        $('.attachment-details').hide()

        $('#add-share-button').click(function(e) {

            e.preventDefault()

            var shareContainer = $("#share-container");
            var shareCount = shareContainer.find(".share").length;
            var shareItem = createShareHtml("socialShare[" + shareCount + "]", "", "", "", "", "_blank");
            shareItem.appendTo(shareContainer);

            //addListeners();
            // $(".tabs").tabs();
        });

        function addTocItem(){
            var index = $('.toc-item').length
            var $item = createTocItem().appendTo("#toc-items")
        }

        function convertPdfToImages(){
            
            var numPages = pdfDocument._pdfInfo.numPages

            convertPdfPageToImage(1, "cover_400", 400)
            convertPdfPageToImage(1, "cover_300", 300)
            convertPdfPageToImage(1, "cover_200", 200)
            convertPdfPageToImage(1, "cover_100", 100)

            for (var i = 1; i <= numPages; i++) {
            
                convertPdfPageToImage(i, i, 1500)

            }

        }

        function convertPdfPageToImage(index, name, size){

            pdfDocument.getPage(index).then(function(page){

                renderPdfPage(page,function(canvas){

                    saveCanvasToServer(canvas, name)

                },size)
            })

        }

        function saveCanvasToServer(canvas, name, onComplete){

            var dataurl = canvas.toDataURL("image/jpeg")

            $.ajax({

                type: "POST",
                url: 'admin-ajax.php?page=real3d_flipbook_admin',
                data : {
                    action : 'r3d_save_page',
                    id: options.id,
                    page : name,
                    dataurl : dataurl,
                    security : options.security
                },

                success: function(response, textStatus, jqXHR) {

                    onComplete(r3d_stripslashes(response))

                },

                error: function(XMLHttpRequest, textStatus, errorThrown) {

                    // console.log(creatingPage, XMLHttpRequest, textStatus, errorThrown)

                }

            })

        }

        function selectJpgImages() {

            if (getOptionValue('pdfUrl') != '') {

                clearPages()
                options.pages = []

            }

            // setOptionValue('type', 'jpg')

            setOptionValue('pdfUrl', '')

            $('.attachment-details').hide()

            var pdf_uploader = wp.media({
                title: 'Select images',
                button: {
                    text: 'Send to Flipbook'
                },
                library: { type: ['image' ]},
                multiple: true // Set this to true to allow multiple files to be selected
            }).on('select', function() {

                var arr = pdf_uploader.state().get('selection');
                var pages = new Array();

                for (var i = 0; i < arr.models.length; i++) {
                    var url = arr.models[i].attributes.sizes.full.url;
                    var thumb = (typeof(arr.models[i].attributes.sizes.medium) != "undefined") ? arr.models[i].attributes.sizes.medium.url : url;
                    var title = arr.models[i].attributes.title;
                    pages.push({
                        title: title,
                        url: url,
                        thumb: thumb
                    });
                }

                var pagesContainer = $("#pages-container");

                for (var i = 0; i < pages.length; i++) {

                    var pagesCount = pagesContainer.find(".page").length;
                    var pageItem = createPageHtml(i + 1, pages[i].title, pages[i].url, pages[i].thumb, "");
                    
                    pageItem.appendTo(pagesContainer);
                    pageItem.hide().fadeIn();

                    pageItem.click(function(e) {
                        expandPage($(this).attr('id'))
                    })

                }

                $('.page-delete').show()
                $('.replace-page').show()


                // addListeners();

                clearLightboxThumbnail()

                generateLightboxThumbnail()

                //document.getElementById("real3dflipbook-options-form").submit();

            }).open();

        }

        /**
         * Create and show a dismissible admin notice
         */
        function addNotice( msg ) {
             
            var div = document.createElement( 'div' );
            $(div).addClass( 'notice notice-info' ).css('position', 'relative').fadeIn();
             
            var p = document.createElement( 'p' );
             
            $(p).text( msg ).appendTo($(div));
             
            var b = document.createElement( 'button' );
            $(b).attr( 'type', 'button' ).addClass( 'notice-dismiss' ).appendTo($(div));
         
            var bSpan = document.createElement( 'span' );
            $(bSpan).addClass( 'screen-reader-text' ).text( 'Dismiss this notice' ).appendTo($(b));
             
            var h1 = document.getElementsByTagName( 'h1' )[0];
            h1.parentNode.insertBefore( div, h1.nextSibling);
         
            $(b).click(function () {
                div.parentNode.removeChild( div );
            });
         
        }

        function removeAllNotices(){
            $(".notice").remove()
        }

        function clearPages() {
            $('.page').remove();
        }

        function clearLightboxThumbnail() {
            $("input[name='lightboxThumbnailUrl']").attr('value', '')
            $("img[name='lightboxThumbnailUrl']").attr('src', '')
        }

        function removePage(index) {
            $('#pages-container').find('#' + index).remove();

            $('.attachment-details').hide()
        }

        function addListeners() {
            $('.submitdelete').click(function() {
                $(this).parent().parent().animate({
                    'opacity': 0
                }, 100).slideUp(100, function() {
                    $(this).remove();
                });
                // $('.unsaved').show()
            });

            /*$('.html-content').each(function() {
                $(this).parent().find('.html-content-hidden').val(escape($(this).val()))
            })
            $('.html-content').change(function() {
                $(this).parent().find('.html-content-hidden').val(escape($(this).val()))
            })*/

            /*$('.select-image-button').click(function(e) {
                        e.preventDefault();
                        var imageURLInput = $(this).parent().find("input");
                        tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
                        $("#TB_window,#TB_overlay,#TB_HideSelect").one("unload", function(e) {
                            e.stopPropagation();
                            e.stopImmediatePropagation();
                            return false;
                        });
             window.send_to_editor = function (html) {
             var imgurl = jQuery('img',html).attr('src');
             imageURLInput.val(imgurl);
             tb_remove();
             };
             }); */

            $('.add-pdf-pages-button').click(function(e) {
                e.preventDefault();

                if ($('.page').length == 0 || confirm('All current pages will be lost. Are you sure?')) {

                    selectPdfFile()

                }
            })

            $('.delete-pages-button').click(function(e) {
                e.preventDefault();

                if (confirm('Delete all pages. Are you sure?')) {

                    clearPages()

                    options.pages = []

                }
            })

            $('.select-image-button').click(function(e) {
                e.preventDefault();

                var $input = $(this).parent().find("input")
                var $img = $(this).parent().find("img")

                var pdf_uploader = wp.media({
                    title: 'Select file',
                    button: {
                        text: 'Select'
                    },
                    multiple: false // Set this to true to allow multiple files to be selected
                }).on('select', function() {

                    // $('.unsaved').show()
                    var arr = pdf_uploader.state().get('selection');
                    var selected = arr.models[0].attributes.url

                    $input.val(selected)
                    $img.attr('src', selected)
                }).open();
            })

            $('.generate-thumbnail-button').click(function(e){
                e.preventDefault()
                if(pdfDocument ){
                    
                    setOptionValue('lightboxThumbnailUrl', "")

                    $("input[name='lightboxThumbnailUrl']").attr('value', "")
                
                    pdfDocument.getPage(1).then(function(page){
                        generateLightboxThumbnail(page)
                   })
                }
                else
                    generateLightboxThumbnail()
                
                
            })

            $('.remove-image-button').click(function(e) {
                e.preventDefault();

                var $input = $(this).parent().find("input")
                var $img = $(this).parent().find("img")

                $input.val('')
                $img.attr('src', '')
            })

            $('.delete-all-pages-button').click(function(e) {

                e.preventDefault();

                clearPages()

            });

            $(".update-all-flipbooks").click(function(e){

                e.preventDefault();

                if (confirm('Apply current settings to all flipbooks. Are you sure?')) {

                    $form.find('.spinner').css('visibility', 'visible')

                    $form.find('.save-button').prop('disabled', 'disabled').css('pointer-events', 'none')

                    var data = $form.serialize() + '&action=r3d_save_all&security=' + options.security

                    $.ajax({
                        type: "POST",
                        url: $form.attr('action'), //.replace('admin-ajax','admin'),
                        data: data,
                        success: function(data, textStatus, jqXHR) {

                            $form.find('.spinner').css('visibility', 'hidden')
                            $form.find('.save-button').prop('disabled', '').css('pointer-events', 'auto')

                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            alert("Status: " + textStatus);
                            alert("Error: " + errorThrown);
                        }
                    })

                }

            })

            $('.delete-page').click(function(e) {

                e.preventDefault();

                var index = jQuery('.attachment-details').attr('data-id')

                if (confirm('Delete page ' + index + '. Are you sure?')) {

                    removePage(index)
                }

            });

            $('.add-jpg-pages-button').click(function(e) {
                //open editor to select one or multiple images and create pages from them
                e.preventDefault();

                if (getOptionValue('pdfUrl') != '') {
                    if (confirm('All current pages will be lost. Are you sure?')) {

                        selectJpgImages()

                    }
                } else
                    selectJpgImages()

            });

            $('.pdf-to-images-button').click(function(e) {
                
                convertPdfToImages()

            });


            $('.add-toc-item').click(function(e) {

                e.preventDefault();

                addTocItem()

            });

            $('.toc-delete-all').click(function(e) {

                e.preventDefault();

                if($(".toc-item-wrapper").length == 0 || confirm('Delete current table on contets?'))
                    $("#toc-items").empty();
                
            });

            $('.load-pdf-outline').click(function(e) {

                e.preventDefault();

                if(getOptionValue('pdfUrl') == ''){
                    alert("Only for PDF flipbook")
                    return
                }

                if($(".toc-item-wrapper").length == 0 || confirm('Delete current table on contets?')){

                    var tocContainer = $("#toc-items").empty();

                    pdfDocument.getOutline().then(function(outline){

                        if(outline && outline.length){

                            for (var i = 0; i < outline.length; i++) {

                                var item = outline[i];

                                var tocItem = createTocItem(item.title, item.page, item.items, item.dest);
                                tocItem.appendTo(tocContainer);

                            }

                        }

                    })

                }

            });

            $('.replace-page').click(function(event) {
                replacePage()
            });

        }

        function replacePage() {
            var pdf_uploader = wp.media({
                title: 'Select image',
                button: {
                    text: 'Select'
                },
                library: {
                        type: ['image' ]
                },
                multiple: false // Set this to true to allow multiple files to be selected
            }).on('select', function() {

                var index = getEditingPageIndex()

                var selected = pdf_uploader.state().get('selection').models[0];

                var url = selected.attributes.sizes.full.url;
                var thumb = (typeof(selected.attributes.sizes.medium) != "undefined") ? selected.attributes.sizes.medium.url : null;

                setSrc(index, url)
                setThumb(index, thumb)
                setEditingPageThumb(thumb)

            }).open();
        }

        function selectPdfFile() {

            var pdf_uploader = wp.media({
                title: 'Select PDF',
                button: {
                    text: 'Send to Flipbook'
                },
                library: {
                        type: ['application/pdf' ]
                },
                multiple: false // Set this to true to allow multiple files to be selected
            }).on('select', function() {

                // $('.unsaved').show()
                var arr = pdf_uploader.state().get('selection');
                var pdfUrl = arr.models[0].attributes.url
                // $("input[name='pdfUrl']").attr('value', pdfUrl);

                setOptionValue('type', 'pdf')
                setOptionValue('pdfUrl', pdfUrl)

                $('.attachment-details').hide()

                clearPages()

                clearLightboxThumbnail()

                options.pages = []

                $('#pages-container').removeClass('ui-sortable')

                //we have the pdf url, now use pdf.js to open pdf 
                function getDocumentProgress(progressData) {
                    // console.log(progressData.loaded / progressData.total);
                    $('.creating-page').html('Loading PDF ' + parseInt(100 * progressData.loaded / progressData.total) + '% ')
                    $('.creating-page').show()

                }

                previewPDFPages(pdfUrl)

            }).open();
        }

        function createTocItem(title, page, items, dest) {

            if (title == 'undefined' || typeof(title) == 'undefined')
                title = ''
            title = r3d_stripslashes(title)

            if (page == 'undefined' || typeof(page) == 'undefined')
                page = ''
            
            var $itemWrapper = $('<div class="toc-item-wrapper">')
            // var $toggle = $('<span>+</span>').appendTo($itemWrapper)
            var $item = $('<div class="toc-item"><input type="text" class="toc-title" placeholder="Title" value="'+title+'"></input><span> : </span><input type="number" placeholder="Page number" class="toc-page" value="'+page+'"></input></div>').appendTo($itemWrapper)
            
            if(dest){
                pdfDocument.getPageIndex(dest[0]).then(function(index){
                    $item.children('.toc-page').val(index + 1)
                })
            }

            var $controls = $('<div>').addClass('toc-controls').appendTo($item)
            // var $btnAddSubItem = $('<button type="button" class="button-secondary toc-add-sub">Add sub item</button>')
            var $btnAddSubItem = $('<span>').addClass('toc-add-sub fa fa-plus').attr('title','Add sub item')
            .appendTo($controls)
            .click(function(){
                // console.log(this)
                var $subItem = createTocItem().appendTo($itemWrapper).addClass('toc-sub-item')
                // var $toggle = $('<span>').addClass('toc-toggle fa fa-caret-right').prependTo($subItem)
            })
            var $btnDelete = $('<span>').addClass('fa fa-times toc-delete').attr('title', 'Delete itemm')
            .appendTo($controls)
            .click(function(){
                if($itemWrapper.find('.toc-item-wrapper').length == 0 || confirm('Delete item and all children') ){

                    $itemWrapper.fadeOut(300,function(){$(this).remove()})
                }
            })

            if(items){
                for (var i = 0; i < items.length; i++) {
                    var item = items[i]
                    var $subItem = createTocItem(item.title, item.page, item.items, item.dest).appendTo($itemWrapper).addClass('toc-sub-item')
                }
            }

            return $itemWrapper.fadeIn()

        }

        function createPageHtml(id, title, src, thumb, htmlContent) {

            htmlContent = unescape(htmlContent);

            if (htmlContent == 'undefined' || typeof(htmlContent) == 'undefined')
                htmlContent = ''

            if (title == 'undefined' || typeof(title) == 'undefined')
                title = ''

            title = r3d_stripslashes(title)

            var res = $('<li id="' + id + '"class="page">' + '<div class="page-img"><img src="' + thumb + '"></div>' + '<p class="page-title">' + id + '</p>' + '<div style="display:block;">' + '<input id="page-title" type="hidden" placeholder="title" value="' + title + '" readonly/>' + '<input id="page-src" type="hidden" placeholder="src" value="' + src + '" readonly/>' + '<input id="page-thumb" type="hidden" placeholder="thumb" value="' + thumb + '" readonly/>' + '<input id="page-html" type="hidden" placeholder="htmlContent" class="html-content-hidden" value="' + escape(htmlContent) + '"readonly/>' + '</div>' + '</li>');

            res.find('img').bind('load', function() {
                var h = $(this).height()
                var w = $(this).width()
                var ch = res.find('.page-img').height()
                // res.find('.page-img').css('width', ch * w / h + 'px')
            })

            var $del = $('<span>').addClass('fa fa-times page-delete').appendTo(res).click(function(e){
                
                e.preventDefault();

                if (confirm('Delete page ' + id + '. Are you sure?')) {

                    removePage(id)
                }


            })

            return res
        }

        function createShareHtml(prefix, id, name, icon, url, target) {

            if (typeof(target) == 'undefined' || target != "_self")
                target = "_blank";

            var markup = $('<div id="' + id + '"class="share">' + '<h4>Share button ' + id + '</h4>' + '<div class="tabs settings-area">' + '<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">' + '<li><a href="#tabs-1">Icon name</a></li>' + '<li><a href="#tabs-2">Icon css class</a></li>' + '<li><a href="#tabs-3">Link</a></li>' + '<li><a href="#tabs-4">Target</a></li>' + '</ul>' + '<div id="tabs-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom">' + '<div class="field-row">' + '<input id="page-title" name="' + prefix + '[name]" type="text" placeholder="Enter icon name" value="' + name + '" />' + '</div>' + '</div>' + '<div id="tabs-2" class="ui-tabs-panel ui-widget-content ui-corner-bottom">' + '<div class="field-row">' + '<input id="image-path" name="' + prefix + '[icon]" type="text" placeholder="Enter icon CSS class" value="' + icon + '" />' + '</div>' + '</div>' + '<div id="tabs-3" class="ui-tabs-panel ui-widget-content ui-corner-bottom">' + '<div class="field-row">' + '<input id="image-path" name="' + prefix + '[url]" type="text" placeholder="Enter link" value="' + url + '" />' + '</div>' + '</div>' + '<div id="tabs-4" class="ui-tabs-panel ui-widget-content ui-corner-bottom">' + '<div class="field-row">' // + '<input id="image-path" name="'+prefix+'[target]" type="text" placeholder="Enter link" value="'+target+'" />'

                +
                '<select id="social-share" name="' + prefix + '[target]">' // + '<option name="'+prefix+'[target]" value="_self">_self</option>'
                // + '<option name="'+prefix+'[target]" value="_blank">_blank</option>'
                +
                '</select>' + '</div>' + '</div>' + '<div class="submitbox deletediv"><span class="submitdelete deletion">x</span></div>' + '</div>' + '</div>' + '</div>');

            var values = ["_self", "_blank"];
            var select = markup.find('select');

            for (var i = 0; i < values.length; i++) {
                var option = $('<option name="' + prefix + '[target]" value="' + values[i] + '">' + values[i] + '</option>').appendTo(select);
                if (typeof(options["socialShare"][id]) != 'undefined') {
                    if (options["socialShare"][id]["target"] == values[i]) {
                        option.attr('selected', 'true');
                    }
                }
            }

            return markup;
        }

        function getOptionValue(optionName, type) {
            var type = type || 'input'
            var opiton = $(type + "[name='" + optionName + "']")
            return opiton.attr('value') || options.globals[optionName];
        }

        function getOption(optionName, type) {
            var type = type || 'input'
            var opiton = $(type + "[name='" + optionName + "']")
            return opiton;
        }

        // console.log(getOptionValue('mode', 'select'))

        function onModeChange() {
            if (getOptionValue('mode', 'select') == 'lightbox')
                $('[href="#tab-lightbox"]').closest('li').show();
            else
                $('[href="#tab-lightbox"]').closest('li').hide();
        }

        getOption('mode', 'select').change(onModeChange)
        onModeChange()

        function onViewModeChange() {
            if (getOptionValue('viewMode', 'select') == 'webgl')
                $('[href="#tab-webgl"]').closest('li').show();
            else
                $('[href="#tab-webgl"]').closest('li').hide();
        }

        getOption('viewMode', 'select').change(onViewModeChange)
        onViewModeChange()

        function setOptionValue(optionName, value, type) {
            var type = type || 'input'
            var $elem = $(type + "[name='" + optionName + "']").attr('value', value).prop('checked', value);

            $("select[name='" + optionName + "']").val(value);
            // $("option[value='"+value+"']").attr("selected", true);

            return $elem
        }

        function setColorOptionValue(optionName, value) {
            var $elem = $("input[name='" + optionName + "']").attr('value', value);
            $elem.wpColorPicker()
            return $elem
        }

        function renderPdfPage(pdfPage, onComplete, height) {
            var context, scale, viewport, canvas, context, renderContext;

            viewport = pdfPage.getViewport(1);
            scale = (height || 80) / viewport.height
            viewport = pdfPage.getViewport(scale);
            canvas = document.createElement('canvas');
            context = canvas.getContext('2d');
            canvas.width = viewport.width
            canvas.height = viewport.height

            renderContext = {
                canvasContext: context,
                viewport: viewport,
                intent: 'display' // intent:'print'
            };

            pdfPage.cleanupAfterRender = true

            pdfPage.render(renderContext).then(function() {

                onComplete(canvas)
            })
        }

        function generateLightboxThumbnail(pdfPage) {
            // var thumb = $("input[name='pages[0][thumb]']").attr('value')

            var thumb = $($('.page')[0]).find('#page-thumb').attr('value')
            var lightboxThumbnailUrl = $("input[name='lightboxThumbnailUrl']").attr('value')
            if (lightboxThumbnailUrl == "") {
                // console.log("generate lightbox thumbnail")
                if (!pdfPage) {
                    $("input[name='lightboxThumbnailUrl']").attr('value', thumb)
                    $("img[name='lightboxThumbnailUrl']").attr('src', thumb)
                } else {
                    var height = getOptionValue('lightboxThumbnailHeight');
                    var viewport = pdfPage.getViewport(1);
                    var scale = height / viewport.height
                    var viewport = pdfPage.getViewport(scale);
                    var canvas = document.createElement('canvas');
                    var context = canvas.getContext('2d');
                    canvas.width = viewport.width
                    canvas.height = viewport.height

                    renderContext = {
                        canvasContext: context,
                        viewport: viewport,
                        intent: 'display' // intent:'print'
                    };

                    pdfPage.cleanupAfterRender = true

                    pdfPage.render(renderContext).then(function(a, b, c, d) {

                        // var thumb = canvas.toDataURL()

                        saveCanvasToServer(canvas, "thumb", function(thumbUrl){
                            $("input[name='lightboxThumbnailUrl']").attr('value', thumbUrl)
                            $("img[name='lightboxThumbnailUrl']").attr('src', thumbUrl)

                        })
                    })

                }   
            }

        }

        function expandPage(index) {

            $('.page').removeClass('page-selected')
            getPage(index).addClass('page-selected')

            $('.attachment-details').show().attr('data-id', index)
            $('.attachment-details h2').text('Edit page ' + index)

            var thumb = getThumb(index)
            if (thumb) {
                $('.delete-page').show()
                $('.replace-page').show()
                $('#edit-page-img').show()
                setEditingPageThumb(thumb)
            } else if(options.pdfUrl != ""){
                    pdfDocument.getPage(Number(index)).then(function(pdfPage){
                            renderPdfPage(pdfPage, function(canvas){
                                    setEditingPageThumbCanvas(canvas)
                            }, 300)
                    })
                

            }else {
                $('.delete-page').hide()
                $('.replace-page').hide()
                $('#edit-page-img').hide()
            }

            setEditingPageTitle(getTitle(index))
            setEditingPageHtmlContent(unescape(getHtmlContent(index)))

        }

        function setEditingPageIndex(index) {
            $('.attachment-details').attr('data-id', index)
        }

        function getEditingPageIndex() {
            return $('.attachment-details').attr('data-id')
        }

        function setEditingPageTitle(title) {
            $('#edit-page-title').val(title)
        }

        function getEditingPageTitle() {
            return $('#edit-page-title').val()
        }

        function setEditingPageSrc(val) {
            $('#edit-page-src').val(val)
        }

        function getEditingPageSrc() {
            return $('#edit-page-src').val()
        }

        function setEditingPageThumb(val) {
            $('#edit-page-canvas').empty()
            $('#edit-page-thumb').val(val)
            $('#edit-page-img').attr('src', val)
        }

        function setEditingPageThumbCanvas(canvas) {
            $('#edit-page-canvas').empty()
            $('#edit-page-img').attr('src', '')
            $(canvas).appendTo($('#edit-page-canvas'))
        }

        function getEditingPageThumb() {
            return $('#edit-page-thumb').val()
        }

        function setEditingPageHtmlContent(htmlContent) {
            $('#edit-page-html-content').val(htmlContent)
        }

        function getEditingPageHtmlContent() {
            return $('#edit-page-html-content').val()
        }

        function getPage(index) {
            return $('#pages-container li[id="' + index + '"]')
        }

        function getTitle(index) {
            return getPage(index).find('#page-title').val()
        }

        function setTitle(index, val) {
            getPage(index).find('#page-title').val(val)
        }

        function getSrc(index) {
            return getPage(index).find('#page-src').val()
        }

        function setSrc(index, val) {
            getPage(index).find('#page-src').val(val)
        }

        function getThumb(index) {
            return getPage(index).find('#page-thumb').val()
        }

        function setThumb(index, val) {
            getPage(index).find('#page-thumb').val(val)
            // getPage(index).find('.page-img').find('img').attr('src', val)
            getPage(index).find('.page-img').css('background', 'url("' + val + '")')
        }

        function getHtmlContent(index) {
            return getPage(index).find('.html-content-hidden').val()
        }

        function setHtmlContent(index, val) {
            getPage(index).find('.html-content-hidden').val(val)
        }

        $('#edit-page-title').bind('change keyup paste', function() {

            var dataId = $(this).parent().parent().attr('data-id')
            setTitle(dataId, $(this).val())

        })

        $('#edit-page-html-content').bind('change keyup paste', function() {

            var dataId = $(this).parent().parent().attr('data-id')
            setHtmlContent(dataId, escape($(this).val()))

        })

        function loadPageFromPdf(pdf, pageIndex) {

            if (!pdf.pageScale) {
                pdf.getPage(1).then(function(page) {
                    var v = page.getViewport(1)

                    //pdf.pageScale = v.width > v.height ? 2048/v.width : 2048/v.height 
                    pdf.pageScale = v.height / 150
                    pdf.thumbScale = v.height / 150

                    createEmptyPages(pdf)

                    /*var lightboxThumbnailUrl = $("input[name='lightboxThumbnailUrl']").attr('value')
                    if (lightboxThumbnailUrl == "") {
                        generateLightboxThumbnail(page)
                    }*/

                    loadPageFromPdf(pdf, pageIndex)

                })
                return
            }

            pdf.getPage(creatingPage).then(function getPage(page) {

                var pagesContainer = $("#pages-container");

                renderPdfPage(page, function(canvas) {

                    // var pagesCount = pagesContainer.find(".page").length;
                    // var currentPage = pagesCount + 1;
                    // var title = options.pages && options.pages[currentPage - 1] ? options.pages[currentPage - 1].title : ''
                    // var htmlContent = options.pages && options.pages[currentPage - 1] ? options.pages[currentPage - 1].htmlContent : ''

                    // var pageItem = createPageHtml("pages[" + pagesCount + "]", currentPage, title, "", "", htmlContent);
                    var pageItem = $("#pages-container").find("#" + creatingPage)
                    // debugger

                    // console.log(pageItem[0])
                    pageItem.find('.page-img').empty().append($(canvas))

                    // pageItem.find('.page-img').width($(canvas).width())

                    // pageItem.appendTo(pagesContainer).click(function(e) {
                    //     expandPage($(this).attr("id"))
                    // })

                    if (creatingPage < pdf._pdfInfo.numPages) {

                        creatingPage++
                        loadPageFromPdf(pdf)

                    }

                })

            })
        }

        function createEmptyPages(pdf) {
            
            var numPages = pdf._pdfInfo.numPages
            var pagesContainer = $("#pages-container");

            pdf.getPage(1).then(function(page) {
                var v = page.getViewport(1)

                for (var i = 1; i <= numPages; i++) {
                    var title = options.pages && options.pages[i - 1] ? options.pages[i - 1].title : ''
                    var htmlContent = options.pages && options.pages[i - 1] ? options.pages[i - 1].htmlContent : ''
                    var pageItem = createPageHtml(i, title, "", "", htmlContent);
                    pageItem.find('.page-img').css('min-width', 80 * v.width / v.height + 'px')
                    pageItem.find('.page-img').empty()
                    pageItem.appendTo(pagesContainer).click(function(e) {
                        expandPage($(this).attr("id"))
                    })

                }

                $('.page-delete').hide()
                $('.replace-page').hide()


            })

        }

    });
})(jQuery);

function r3d_stripslashes(str) {
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Ates Goral (http://magnetiq.com)
    // +      fixed by: Mick@el
    // +   improved by: marrtins
    // +   bugfixed by: Onno Marsman
    // +   improved by: rezna
    // +   input by: Rick Waldron
    // +   reimplemented by: Brett Zamir (http://brett-zamir.me)
    // +   input by: Brant Messenger (http://www.brantmessenger.com/)
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: stripslashes('Kevin\'s code');
    // *     returns 1: "Kevin's code"
    // *     example 2: stripslashes('Kevin\\\'s code');
    // *     returns 2: "Kevin\'s code"
    return (str + '').replace(/\\(.?)/g, function(s, n1) {
        switch (n1) {
            case '\\':
                return '\\';
            case '0':
                return '\u0000';
            case '':
                return '';
            default:
                return n1;
        }
    });
}